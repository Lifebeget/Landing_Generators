import Router from 'vue-router'
import Axios from 'axios'

export async function createRouter(ssrContext, createDefaultRouter, routerOptions, config) {
	const options = routerOptions ? routerOptions : createDefaultRouter(ssrContext, config).options
	const CMS_REST_API = config?.CMS_REST_API;

	let LOCALE;
	switch (config.COUNTRY) {
		case 'mx':
			LOCALE = 'es-mx'
			break;

		default:
			LOCALE = 'pt-br'
			break;
	}

	const response = await Axios({
		method: 'post',
		url: CMS_REST_API?  `${CMS_REST_API}/public/directions/`: `https://cms.ebaconline.com.br/api/public/directions/`,
	});
	const directions = response.data.data.data;

	return new Router({
		...options,
		routes: fixRoutes(options.routes, directions, LOCALE),
    scrollBehavior(to, from, savedPosition) {
      if (
        (to.name === `courses___${LOCALE}` && from.name === `courses___${LOCALE}`) ||
        (to.name === `coursesTopic___${LOCALE}` && from.name === `courses___${LOCALE}`) ||
        (to.name === `courses___${LOCALE}` && from.name === `coursesTopic___${LOCALE}`) ||
        (to.name === `coursesTopic___${LOCALE}` && from.name === `coursesTopic___${LOCALE}`) ||
        (to.name === `sale-sale___${LOCALE}` && from.name === `sale-sale___${LOCALE}`) ||
				(to.hash === "#professores")
      ) return;

      return { x: 0, y: 0 };
    }
	});
}

function fixRoutes(defaultRoutes, directions, LOCALE) {
	const dirctionSlugRoutes = [
		{
			name: 'blog',
			slugName: 'blogSlug',
		},
		{
			name: 'webinars',
			slugName: 'eventSlug'
		}
	];

	const slashRoutes = [];
	dirctionSlugRoutes.forEach(dsRoute => {
		slashRoutes.push({
			path: `/${dsRoute.name}/`,
			redirect: `/${dsRoute.name}`,
			pathToRegexpOptions: {
				strict: true
			},
		})
	});

	const routes = defaultRoutes.map((route) => {
		dirctionSlugRoutes.forEach(dsRoute => {
			if (route.name === `${dsRoute.name}-direction___${LOCALE}`) {
				route.beforeEnter = function (to, from, next) {
					const findedIndex = directions.findIndex(i => i[dsRoute.slugName] === to.params.direction)
					if (findedIndex !== -1 || !to.params.direction) {
						return next()
					}
					else return next({ name: `${dsRoute.name}-slug___${LOCALE}`, params: { slug: to.params.direction }, query: { ...to.query }, replace: true })
				}
				route.pathToRegexpOptions = {
					strict: true
				};
			}
			if (route.name === `${dsRoute.name}-slug___${LOCALE}`) {
				route.beforeEnter = function (to, from, next) {
					if (!to.params.slug) return next({name: `${dsRoute.name}-direction___${LOCALE}`})
					else return next();
				}
				route.pathToRegexpOptions = {
					strict: true
				};
			}
		});
		return route;
	})

	const customRoutes = [
	]
	return [ ...slashRoutes, ...routes, ...customRoutes ]
}
