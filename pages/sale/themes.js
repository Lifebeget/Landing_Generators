const THEMES = {
  default: {
    'primary': '#905DB4',
    'primary-antagonist': '#FFF',
    'primary-active': '#7733A7',

    'primary-alternative': '#EDE2F8',
    'primary-alternative-antagonist': '#000000',
    'primary-alternative-active': '#EDE2F8',

    'secondary': '#EB569A',
    'secondary-antagonist': '#FFF',
    'secondary-active': '#F62484',

    'accent': '#FE8800',
    'accent-antagonist': '#FFF',
    'accent-active': '#FE8800',

    'first-section-background': '#f9f9f9',
    'section-background': '#FFF',

    'h1-text-color': '#000',
    'main-text-color': '#000',
    'secondary-text-color': '#fff',
  },
  dark: {
    'primary': '#ccc',
    'primary-antagonist': '#FFF',
    'primary-active': '#b6b6b6',

    'primary-alternative': '#EDE2F8',
    'primary-alternative-antagonist': '#000000',
    'primary-alternative-active': '#EDE2F8',

    'secondary': '#EB569A',
    'secondary-antagonist': '#FFF',
    'secondary-active': '#F62484',

    'accent': '#FE8800',
    'accent-antagonist': '#FFF',
    'accent-active': '#FE8800',

    'first-section-background': '#1A2023',
    'section-background': '#263238',

    'h1-text-color': '#fff',
    'main-text-color': '#FFF',
    'secondary-text-color': '#FFF',
  }
}

export default ({ themeName }) => {
  return {
    ...Object.entries(THEMES[themeName]).reduce((acc, [name, value]) => ({ ...acc, [`--sale-${name}`]: value }), {})
  }
}
