export const getEternalEndDate = () => {
  try {
    let day = (new Date).getDay();
    let nextSaleEndDate = null;
    switch (day) {
      case 0: nextSaleEndDate = nextDate(1); break; // SUN
      case 1: nextSaleEndDate = new Date(new Date().setHours(23, 59, 59, 999)); break; // MON
      case 2: nextSaleEndDate = nextDate(5); break; // TUE
      case 3: nextSaleEndDate = nextDate(5); break; // WED
      case 4: nextSaleEndDate = nextDate(5); break; // THU
      case 5: nextSaleEndDate = new Date(new Date().setHours(23, 59, 59, 999)); break; // FRI
      case 6: nextSaleEndDate = nextDate(1); break; // SAT
    }

    return nextSaleEndDate;
  } catch(e) {
    console.error(e);
  }
};

const nextDate = (dayIndex) => {
  let today = new Date();
  today.setDate(today.getDate() + (dayIndex - 1 - today.getDay() + 7) % 7 + 1);
  return new Date(today.setHours(23, 59, 59, 999));
};
