export const ambassadors = [
  {
    fullName: 'Bruno Campos De Oliveira',
    intro: 'Сo-fundou a CTRL.365',
    description: `Em mais de 10 anos empreendendo e atuando no mercado de Marketing Digital,
      Bruno co-fundou a CTRL.365 e a AdsPlay, onde gerenciou projetos para mais
      de 150 agências em mais de 2.500 campanhas na América Latina, Caribe,
      África e Europa. Entre seus clientes estiveram gigantes como NET, Land
      Rover, 3 Corações, Cobasi, iFood, Mentos, Sebrae, Jaguar, Samsung, Johnson
      & Johnson, Vale, Bradesco, Nextel, Coca-Cola, Motorola, Timberland, Anima
      Educação, Sofisa, Olla, MSC, Nextel, KPMG, Drogaria SP, Amil entre outras.`,
    avatar: {
      src: require('@/assets/images/ambassadors/BrunoCamposDeOliveira@1x.jpg'),
      srcset: [
        `${ require('@/assets/images/ambassadors/BrunoCamposDeOliveira@1x.jpg') } 64w`,
        `${ require('@/assets/images/ambassadors/BrunoCamposDeOliveira@2x.jpg') } 276w`,
        `${ require('@/assets/images/ambassadors/BrunoCamposDeOliveira@3x.jpg') } 350w`,
      ].join(', '),
    },
  },
  {
    fullName: 'Matheus Marques',
    intro: 'Arquiteto e CEO do Hiperstudio',
    description: `Formado em Arquitetura e Urbanismo pela Universidade Federal do Paraná,
      possui mais de 14 anos de experiência e mais de 20 premiações por seus trabalhos como
      arquiteto. É sócio-fundador do Hiperstudio, um escritório de arquitetura que se dedica
      a projetos inovadores atrelados à responsabilidade cultural, econômica e ambiental.
      É reconhecido nacional e internacionalmente por seus projetos, em concursos de
      arquitetura e prêmios de sustentabilidade.`,
    avatar: {
      src: require('@/assets/images/ambassadors/MatheusMarques@1x.jpg'),
      srcset: [
        `${ require('@/assets/images/ambassadors/MatheusMarques@1x.jpg') } 64w`,
        `${ require('@/assets/images/ambassadors/MatheusMarques@2x.jpg') } 276w`,
        `${ require('@/assets/images/ambassadors/MatheusMarques@3x.jpg') } 350w`,
      ].join(', '),
    },
  },
];
