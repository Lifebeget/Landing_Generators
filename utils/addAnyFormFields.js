export default async function addAnyFormFields({ ref }) {
  if (!window.gaData) {
    setTimeout(() => addAnyFormFields({ ref }), 1000);
    return;
  }
  const formSelector = ref.$el.querySelector('form');

  try {
    const cloudflareData = await this.$axios({
      method: 'get',
      url: `https://www.cloudflare.com/cdn-cgi/trace`,
    });
    const ipList = cloudflareData.data.match(/ip=.*/gm);
    formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="ClientIP" value="' + (ipList && ipList[0]) + '">');
  } catch (e) {
    console.error(e);
  }

  try {
    formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="window_height" value="' + window.innerHeight + '">');
    formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="window_width" value="' + window.innerWidth + '">');
    formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="Referrer" value="' + document.referrer + '">');
    formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="PageUrl" value="' + window.location.toString() + '">');
    formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="UserTimestamp" value="' + (new Date()) + '">');
    if (this) formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="UserDate" value="' + (new Date().toLocaleDateString(this.$app.config.current.localeName) + ' ' + new Date().toLocaleTimeString(this.$app.config.current.localeName)) + '">');
    formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="UserAgent" value="' + window.navigator.userAgent + '">');
    if (typeof ga === "function" && typeof ga.getAll === "function") {
      formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="ga_cid" value="' + ga.getAll()[0].get('clientId') + '">');
    }

    if (gaData["UA-165354511-2"] && gaData["UA-165354511-2"].experiments) {
      formSelector.insertAdjacentHTML('beforeend', '<input class="ga_experiments" type="hidden" name="experiments" value="">');
      $('.ga_experiments').val(JSON.stringify(gaData["UA-165354511-2"].experiments));
    }
  } catch (e) {
    console.error(e);
  }

  let currentUrl = window.location.toString(), currentParams = getJsonFromUrl(currentUrl), hiddenFields = [];

  if (this?.$app?.sbjs?.get?.current?.typ) {
    // console.info(`this.$app.sbjs.get.current.typ`, this.$app.sbjs.get.current.typ);

    if (this.$app.sbjs.get.current.src) {
      if (this.$app.sbjs.get.current.src === '(none)') {
        currentParams['utm_source'] = 'not_set';
      } else {
        currentParams['utm_source'] = this.$app.sbjs.get.current.src;
      }
    } else {
      currentParams['utm_source'] = 'not_set';
    }

    if (this.$app.sbjs.get.current.mdm) {
      if (this.$app.sbjs.get.current.mdm === '(none)') {
        currentParams['utm_medium'] = 'not_set';
      } else {
        currentParams['utm_medium'] = this.$app.sbjs.get.current.mdm;
      }
    } else {
      currentParams['utm_medium'] = 'not_set';
    }

    if (this.$app.sbjs.get.current.cmp) {
      if (this.$app.sbjs.get.current.cmp === '(none)') {
        currentParams['utm_campaign'] = 'not_set';
      } else {
        currentParams['utm_campaign'] = this.$app.sbjs.get.current.cmp;
      }
    } else {
      currentParams['utm_campaign'] = 'not_set';
    }

    if (this.$app.sbjs.get.current.cnt) {
      if (this.$app.sbjs.get.current.cnt === '(none)') {
        currentParams['utm_content'] = 'not_set';
      } else {
        currentParams['utm_content'] = this.$app.sbjs.get.current.cnt;
      }
    } else {
      currentParams['utm_content'] = 'not_set';
    }

    if (this.$app.sbjs.get.current.trm) {
      if (this.$app.sbjs.get.current.trm === '(none)') {
        currentParams['utm_term'] = 'not_set';
      } else {
        currentParams['utm_term'] = this.$app.sbjs.get.current.trm;
      }
    } else {
      currentParams['utm_term'] = 'not_set';
    }

    if (this.$app.sbjs.get.session.cpg) {
      if (this.$app.sbjs.get.session.cpg === '(none)') {
        currentParams['current_page'] = 'not_set';
      } else {
        currentParams['current_page'] = this.$app.sbjs.get.session.cpg;
      }
    } else {
      currentParams['current_page'] = 'not_set';
    }

    if (this.$app.sbjs.get.current_add.rf) {
      if (this.$app.sbjs.get.current_add.rf === '(none)') {
        currentParams['referrer_url'] = 'not_set';
      } else {
        currentParams['referrer_url'] = this.$app.sbjs.get.current_add.rf;
      }
    } else {
      currentParams['referrer_url'] = 'not_set';
    }

    if (this.$app.sbjs.get.current_add.ep) {
      if (this.$app.sbjs.get.current_add.ep === '(none)') {
        currentParams['entrance_point'] = 'not_set';
      } else {
        currentParams['entrance_point'] = this.$app.sbjs.get.current_add.ep;
      }
    } else {
      currentParams['entrance_point'] = 'not_set';
    }
    if (this.$app.sbjs.get.udata.vst) {
      if (this.$app.sbjs.get.udata.vst === '(none)') {
        currentParams['visits_count'] = 'not_set';
      } else {
        currentParams['visits_count'] = this.$app.sbjs.get.udata.vst;
      }
    } else {
      currentParams['visits_count'] = 'not_set';
    }

  }

  if (currentParams && Object.keys(currentParams).length > 0) {
    Object.keys(currentParams).map(key => {
      hiddenFields[key] = currentParams[key];
      formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="' + key + '" value="' + currentParams[key] + '">');
    });
  }
}

function getJsonFromUrl(url) {
  if(!url) url = location.href;
  let question = url.indexOf("?");
  let hash = url.indexOf("#");
  if(hash==-1 && question==-1) return {};
  if(hash==-1) hash = url.length;
  let query = question==-1 || hash==question+1 ? url.substring(hash) :
    url.substring(question+1,hash);
  let result = {};
  query.split("&").forEach(function(part) {
    if(!part) return;
    part = part.split("+").join(" "); // replace every + with space, regexp-free version
    let eq = part.indexOf("=");
    let key = eq>-1 ? part.substr(0,eq) : part;
    let val = eq>-1 ? decodeURIComponent(part.substr(eq+1)) : "";
    let from = key.indexOf("[");
    if(from==-1) result[decodeURIComponent(key)] = val;
    else {
      let to = key.indexOf("]",from);
      let index = decodeURIComponent(key.substring(from+1,to));
      key = decodeURIComponent(key.substring(0,from));
      if(!result[key]) result[key] = [];
      if(!index) result[key].push(val);
      else result[key][index] = val;
    }
  });
  return result;
}
