export default (link) => {
  let linkString = String(link || '');
  const ioV = linkString.indexOf('v=');
  if (ioV !== -1) {
    linkString = linkString.slice(ioV + 2)
  } else {
    linkString = /[^/]*$/.exec(linkString)[0]
  }

  let ytId = [];
  for (let i = 0; i < linkString.length; i++) {
    if (linkString[i] !== '&') {
      ytId.push(linkString[i])
    } else break;
  }

  return ytId.join('')
}
