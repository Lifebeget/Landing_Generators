import { customAlphabet } from 'nanoid';
import dictionary from 'nanoid-dictionary';

const nanoid = customAlphabet(dictionary.nolookalikes, 21);

export default () => nanoid();
