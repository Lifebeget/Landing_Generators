export const getFilterFromUrl = ({
  route,
  store,
  topicFilter,
  considerUrlParams = [],
  windowMode = false,
  router
}) => {
  const params = route.params;
  const query = route.query;
  const filter = { ...query };

  let hasWindow = false;

  try {
    hasWindow = !!window;
  } catch (e) {}

  if ((windowMode && !hasWindow) || !windowMode) {
    considerUrlParams.forEach(param => {
      if (params.hasOwnProperty(param)) filter[param] = params[param];
    });

    if (topicFilter) {
      const directionId = store.state.courses.directions.find(
        i => i.slug === filter.courses
      )?.id;
      if (directionId) store.dispatch('topics/getTopics', { directionId });

      if (!!params.topic) {
        filter.topic = [params.topic];
      } else {
        if (query.topic && typeof query.topic === 'string') {
          filter.topic = [query.topic];
          query.topic = [query.topic];
        }
      }
    }
  }

  return filter;
};

export const fillUrlFromFilter = async ({
  router,
  store,
  filter,
  replaceMode = false,
  considerUrlParams = [],
  ignoreFilterParams = [],
  topicFilter
}) => {
  const params = {};
  const query = {};

  considerUrlParams.forEach(param => {
    if (filter.hasOwnProperty(param)) params[param] = filter[param];
  });

  Object.entries(filter).forEach(([key, value]) => {
    if (!ignoreFilterParams.includes(key) && !considerUrlParams.includes(key))
      query[key] = value;
  });

  if (topicFilter) {
    return await fillUrlFromFilter_topic({
      store,
      router,
      filter,
      query,
      params,
      replaceMode
    });
  }

  if (replaceMode) {
    history.pushState(null, null, router.resolve({ params, query }).href);
  } else {
    router.push({ params, query });
  }
};

export const getAppliedFilter = ({ filter, ignoreFilterParams }) => {
  const appliedFilter = [];

  Object.entries(filter).forEach(([key, value]) => {
    if (ignoreFilterParams.includes(key)) return;
    if (Array.isArray(value)) {
      value.forEach(value => {
        appliedFilter.push(`${key}.${value}`);
      });
    } else {
      appliedFilter.push(`${key}.${value}`);
    }
  });

  return appliedFilter;
};

async function fillUrlFromFilter_topic({
  store,
  router,
  filter,
  query,
  params,
  replaceMode
}) {
  let path = router.history.current.path;
  const _filter = { ...filter };
  if (typeof _filter.topic === 'string') _filter.topic = [_filter.topic];

  const selectedButDisabledByDirection = store.state.topics.allList.filter(
    i => i._selectedButDisabledByDirection
  );
  if (selectedButDisabledByDirection.length > 0) {
    for (let i = 0; i < selectedButDisabledByDirection.length; i++) {
      const topic = selectedButDisabledByDirection[i];
      if (!_filter.topic) _filter.topic = [];
      if (!_filter.topic.includes(topic.slug)) {
        _filter.topic.push(topic.slug);
      }
    }
  }
  store.commit('topics/clearSelectedButDisabledByDirection');

  const isDirectionSelected = _filter.courses !== 'cursos';
  const oneTopicSelected = _filter.topic?.length === 1;

  if (isDirectionSelected) {
    const directionId = store.state.courses.directions.find(
      i => i.slug === filter.courses
    )?.id;
    if (directionId) await store.dispatch('topics/getTopics', { directionId });

    if (_filter.topic?.length) {
      for (let i = 0; i < _filter.topic.length; i++) {
        const topicSlug = _filter.topic[i];
        if (
          store.state.topics.list.findIndex(j => j.slug === topicSlug) === -1 &&
          _filter.topic[i]
        ) {
          delete _filter.topic[i];
          // store.commit('topics/setSelectedButDisabledByDirection', { slug: topicSlug, status: true })
        }
      }
    }
  } else {
    await store.commit('topics/restoreTopics');
  }

  if (!isDirectionSelected && oneTopicSelected) {
    delete params.courses;
    delete query.topic;

    params.topic = _filter.topic.filter(i => i)[0];
    if (!!params.topic && !params.topic[0]) delete params.topic;

    path = `/cursos/${_filter.topic[0]}`;
  } else {
    delete params.topic;

    params.courses = _filter.courses || 'cursos';
    const topics = _filter.topic?.filter(i => !!i) || [];
    topics.length ? (query.topic = topics) : delete query.topic;

    if (JSON.stringify(query.topic) === JSON.stringify([])) delete query.topic;

    path = `/${params.courses}`;
  }

  if (replaceMode) {
    history.pushState(null, null, router.resolve({ params, query, path }).href);
  } else {
    router.push({ params, query, path });
  }

  filter = _filter;
}
