export default (s) => {
  if (typeof s !== 'string') return s;
  return s.replace(/<a\s+href=/gi, '<a target="_blank" href=');
}
