import breakpointsMixin from "./breakpoints.mixin"
export default {
  mixins: [breakpointsMixin],
  data() {
    return {
      viewpointElement: null,
      viewpointDefault: "width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0",
      viewpointTablet: "width=1024px, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    }
  },
  watch: {
    'breakpoint': {
      handler(value) {
        if (value === '$tb') {
          this.viewpointElement.setAttribute("content", this.viewpointTablet)
        } else {
          this.viewpointElement.setAttribute("content", this.viewpointDefault)
        }
      },
    }
  },
  mounted() {
    this.viewpointElement = document.querySelector('meta[name="viewport"]')
  },
}