import orderBy from 'lodash/orderBy';

export default {
  data() {
    return {
      basket: [],

      basketLoading: false,
    }
  },
  computed: {
    basketComputed() {
      const twoForOneMode = this.$store.state.sale.sale.modes.twoForOne;
      const basket = this.basket;
      let basketComputed = [];

      if (twoForOneMode) {
        basketComputed = orderBy(basket, ['price.price', 'shortName'], ['desc', 'asc'])
          .map((item, index) => {
            const _item = { ...item };
            _item.free = (index + 1) % 2 === 0;
            return _item;
          });

        return basketComputed;
      } else {
        return basket;
      }
    },
  },
  async created() {
    if (process.client) {
      await this.setBasket();
    }
  },
  methods: {
    async setBasket() {
      const basketFromLocalStorage = this.getBasketFromLocalStorage();

      if (!basketFromLocalStorage.length) return;

      this.basketLoading = true;
      const nomenclatures = await this.getAllNomenclatures();

      this.basket = basketFromLocalStorage.reduce((acc, productFromLocalStorage) => {
        const nomenclature = nomenclatures.find((nomenclature) => nomenclature.id === productFromLocalStorage);

        if (nomenclature) {
          nomenclature.price = this.getProductPrice(nomenclature);
          acc.push(nomenclature);
        }

        return acc;
      }, []);

      this.basketLoading = false;
    },
    async getAllNomenclatures() {
      let response;
      try {
        const latam = ['pe', 'co', 'ar', 'cl'].includes(this.$app?.config?.common?.currentCountry)
        response = await this.$axios({
          method: 'post',
          url: `${this.$nuxt.$config.CMS_REST_API}/public/showcase/`,
          data: {
            saleId: this.$store.state.sale.sale.id,
            latam
          },
        });
      } catch (e) {
        console.error(e);
      }

      if (response?.data?.data?.data) {
        return response.data.data.data;
      } else {
        return [];
      }
    },
    setProductInLocalStorage(product) {
      const basketFromLocalStorage = this.getBasketFromLocalStorage();
      if (!basketFromLocalStorage.includes(product.id)) {
        basketFromLocalStorage.push(product.id);

        localStorage.setItem(`sale-${this.$store.state.sale.sale.id}`, JSON.stringify(basketFromLocalStorage));
      }
    },
    removeProductFromLocalStorage(product) {
      let basketFromLocalStorage = this.getBasketFromLocalStorage();
      if (basketFromLocalStorage.includes(product.id)) {
        basketFromLocalStorage = basketFromLocalStorage.filter((nomenclatureId) => nomenclatureId !== product.id)

        localStorage.setItem(`sale-${this.$store.state.sale.sale.id}`, JSON.stringify(basketFromLocalStorage));
      }
    },
    getBasketFromLocalStorage() {
      const basketString = localStorage.getItem(`sale-${this.$store.state.sale.sale.id}`);
      let basket = [];
      try {
        basket = JSON.parse(basketString) || [];
      } catch (e) {
        console.error(e);
      }

      return basket;
    },
  },
}
