export default {
  data() {
    return {
      breakpoint: null,
      pageWidth: null
    }
  },
  mounted() {
    this.setBreakpoint({ target: window });
    window.addEventListener('resize', this.setBreakpoint);
  },
  methods: {
    setBreakpoint(e) {
      this.pageWidth = e.target.innerWidth;
      this.breakpoint =
        e.target.innerWidth < 480 ? '$xxs' :
          e.target.innerWidth < 768 ? '$xs' :
            e.target.innerWidth < 1024 ? '$tb' :
              e.target.innerWidth < 1280 ? '$md' :
                e.target.innerWidth < 1600 ? '$lp' : '$xl'
    }
  }
}