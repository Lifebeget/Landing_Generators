import urlencodeFormData from "../utils/urlencodeFormData";

const MONTH_NAME = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro'
];

export default {
  data () {
    return {
      form: {
        name: '',
        phone: '',
        promoCode: '',
        acceptedRules: true,
        email: ''
      },
      loading: {
        form: false,
      },
      nextMondayDate: this.getDateOfNextMonday()
    }
  },
  methods: {
    getJsonFromUrl(url) {
      if(!url) url = location.href;
      let question = url.indexOf("?");
      let hash = url.indexOf("#");
      if(hash==-1 && question==-1) return {};
      if(hash==-1) hash = url.length;
      let query = question==-1 || hash==question+1 ? url.substring(hash) :
        url.substring(question+1,hash);
      let result = {};
      query.split("&").forEach(function(part) {
        if(!part) return;
        part = part.split("+").join(" "); // replace every + with space, regexp-free version
        let eq = part.indexOf("=");
        let key = eq>-1 ? part.substr(0,eq) : part;
        let val = eq>-1 ? decodeURIComponent(part.substr(eq+1)) : "";
        let from = key.indexOf("[");
        if(from==-1) result[decodeURIComponent(key)] = val;
        else {
          let to = key.indexOf("]",from);
          let index = decodeURIComponent(key.substring(from+1,to));
          key = decodeURIComponent(key.substring(0,from));
          if(!result[key]) result[key] = [];
          if(!index) result[key].push(val);
          else result[key][index] = val;
        }
      });
      return result;
    },
    getDateOfNextMonday() {
      const isValidDate = (date) => date instanceof Date && !isNaN(date);
      const dateParse = (date) => date ? new Date(date) : null;
      const dateCheck = (date, endDate) => date < endDate;
      const endOfWeek = (date) => {
        const lastDay = (7 - date.getDay()) % 7;
        const nextWeek = date.getDate() + lastDay + 1;
        return new Date(date.setDate(nextWeek));
      }

      let nextEndDate;
      const currentDate = new Date();

      if (this.date.autoDate) {
        nextEndDate = endOfWeek(currentDate);
      } else {
        const date = dateParse(this.date.startDate);
        if (isValidDate(date) && dateCheck(currentDate, date)) {
          nextEndDate = date;
        } else {
          nextEndDate = endOfWeek(currentDate);
        }
      }

      const nextEndDateText = nextEndDate.getDate() + ' de ' + MONTH_NAME[nextEndDate.getMonth()];
      return nextEndDateText;
    },
    async onSubmit() {
      if (this.loading.form) return;

      this.loading.form = true;
      const isValid = await this.$refs.form.validate();

      if (!isValid) {
        this.loading.form = false;
        return;
      }

      const formData = new FormData(this.$refs.form.$el.querySelector('form'));
      formData.append('name', this.name);
      formData.append('phone', this.phone);
      formData.append('email', this.email);
      formData.append('checkbox', this.acceptedRules);
      formData.append('promo_code', this.promoCode.trim().toLocaleLowerCase().replace(/ /g, ''));

      try {
        const response = await this.$axios({
          method: 'post',
          url: this.$app.config.current.leadFormUrl,
          data: urlencodeFormData(formData),
          headers: {
            'Content-Type': `application/x-www-form-urlencoded`,
          },
        });

        this.successDialog = true;

        this.$refs.form.reset();
        this.name = '';
        this.phone = '';
        this.email = '';
        this.promoCode = '';

        if (response.data && response.data.hasOwnProperty('status') && response.data.status === 'ok') {
          this.$router.push({
            path: `/${`${this.$route.path}/thankyou`.split('/').filter(String).join('/')}`,
            ...response.data.context && {
              query: {
                context: response.data.context,
              },
            },
          });
        }

        // this.$gtm.push({ 'event': 'autoEvent', 'eventCategory': 'lead', 'eventAction': 'index_footer' });
      } catch (e) {
        console.error(e);
      }

      this.loading.form = false;
    },
    async addAnyFormFields({ ref  }) {
      if (!window.gaData) {
        setTimeout(() => this.addAnyFormFields({ ref }), 1000);
        return;
      }
      const formSelector = ref.$el.querySelector('form');

      try {
        const cloudflareData = await this.$axios({
          method: 'get',
          url: `https://www.cloudflare.com/cdn-cgi/trace`,
        });
        const ipList = cloudflareData.data.match(/ip=.*/gm);
        formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="ClientIP" value="'+(ipList && ipList[0])+'">');
      } catch (e) {
        console.error(e);
      }

      try {
        formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="window_height" value="'+window.innerHeight+'">');
        formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="window_width" value="'+window.innerWidth+'">');
        formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="Referrer" value="'+document.referrer+'">');
        formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="PageUrl" value="'+window.location.toString()+'">');
        formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="UserTimestamp" value="'+(new Date())+'">');
        formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="UserDate" value="'+(new Date().toLocaleDateString('pt-br') + ' ' + new Date().toLocaleTimeString('pt-br'))+'">');
        formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="UserAgent" value="'+window.navigator.userAgent+'">');
        if(typeof ga === "function" && typeof ga.getAll === "function") {
          formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="ga_cid" value="'+ga.getAll()[0].get('clientId')+'">');
        }

        if(gaData["UA-165354511-2"] && gaData["UA-165354511-2"].experiments) {
          formSelector.insertAdjacentHTML('beforeend', '<input class="ga_experiments" type="hidden" name="experiments" value="">');
          $('.ga_experiments').val(JSON.stringify(gaData["UA-165354511-2"].experiments));
        }
      } catch(e) {
        console.error(e);
      }

      let currentUrl = window.location.toString(), currentParams = this.getJsonFromUrl(currentUrl), hiddenFields = [];

      if (this?.$app?.sbjs?.get?.current?.typ) {

        if(this.$app.sbjs.get.current.src) {
          if(this.$app.sbjs.get.current.src === '(none)') {
            currentParams['utm_source'] = 'not_set';
          } else {
            currentParams['utm_source'] = this.$app.sbjs.get.current.src;
          }
        } else {
          currentParams['utm_source'] = 'not_set';
        }

        if(this.$app.sbjs.get.current.mdm) {
          if(this.$app.sbjs.get.current.mdm === '(none)') {
            currentParams['utm_medium'] = 'not_set';
          } else {
            currentParams['utm_medium'] = this.$app.sbjs.get.current.mdm;
          }
        } else {
          currentParams['utm_medium'] = 'not_set';
        }

        if(this.$app.sbjs.get.current.cmp) {
          if(this.$app.sbjs.get.current.cmp === '(none)') {
            currentParams['utm_campaign'] = 'not_set';
          } else {
            currentParams['utm_campaign'] = this.$app.sbjs.get.current.cmp;
          }
        } else {
          currentParams['utm_campaign'] = 'not_set';
        }

        if(this.$app.sbjs.get.current.cnt) {
          if(this.$app.sbjs.get.current.cnt === '(none)') {
            currentParams['utm_content'] = 'not_set';
          } else {
            currentParams['utm_content'] = this.$app.sbjs.get.current.cnt;
          }
        } else {
          currentParams['utm_content'] = 'not_set';
        }

        if(this.$app.sbjs.get.current.trm) {
          if(this.$app.sbjs.get.current.trm === '(none)') {
            currentParams['utm_term'] = 'not_set';
          } else {
            currentParams['utm_term'] = this.$app.sbjs.get.current.trm;
          }
        } else {
          currentParams['utm_term'] = 'not_set';
        }

        if(this.$app.sbjs.get.session.cpg) {
          if(this.$app.sbjs.get.session.cpg === '(none)') {
            currentParams['current_page'] = 'not_set';
          } else {
            currentParams['current_page'] = this.$app.sbjs.get.session.cpg;
          }
        } else {
          currentParams['current_page'] = 'not_set';
        }

        if(this.$app.sbjs.get.current_add.rf) {
          if(this.$app.sbjs.get.current_add.rf === '(none)') {
            currentParams['referrer_url'] = 'not_set';
          } else {
            currentParams['referrer_url'] = this.$app.sbjs.get.current_add.rf;
          }
        } else {
          currentParams['referrer_url'] = 'not_set';
        }

        if(this.$app.sbjs.get.current_add.ep) {
          if(this.$app.sbjs.get.current_add.ep === '(none)') {
            currentParams['entrance_point'] = 'not_set';
          } else {
            currentParams['entrance_point'] = this.$app.sbjs.get.current_add.ep;
          }
        } else {
          currentParams['entrance_point'] = 'not_set';
        }
        if(this.$app.sbjs.get.udata.vst) {
          if(this.$app.sbjs.get.udata.vst === '(none)') {
            currentParams['visits_count'] = 'not_set';
          } else {
            currentParams['visits_count'] = this.$app.sbjs.get.udata.vst;
          }
        } else {
          currentParams['visits_count'] = 'not_set';
        }

      }

      if(currentParams && Object.keys(currentParams).length > 0) {
        Object.keys(currentParams).map(key => {
          hiddenFields[key] = currentParams[key];
          formSelector.insertAdjacentHTML('beforeend', '<input type="hidden" name="'+key+'" value="'+currentParams[key]+'">');
        });
      }
    },
  }
}
