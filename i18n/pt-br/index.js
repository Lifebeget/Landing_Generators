import meta from './meta';
export default {
  ...meta,
  'direction.courses': 'Todos',
  'direction.coursesMobile': 'Todas as áreas',
  'direction.0.metaDescription': 'Cursos online com certificados válidos em todo Brasil. Os professores de melhores práticas irão ensinar-lhe todos os segredos das profissões digitais.',
  'direction.0.metaTitle': 'Cursos online com Certificado — EBAC Online',
  'direction.0.seoText': 'Cursos online com certificados válidos em todo Brasil. Os professores de melhores práticas irão ensinar-lhe todos os segredos das profissões digitais.',
  'direction.0.title': 'Cursos online',
  'types.any': 'Todos',
  'types.course': 'Curso',
  'types.profession': 'Profissão',
  'month': 'sem meses | {count} mês | {count} meses',
  'course': 'cursos | curso | cursos',
  'profession': 'profissões | profissão | profissões',
  'day': '0 dias | 1 dia | {days} dias',
  'and': 'e',
  'required': 'Preenchimento obrigatório',
  'min': 'Comprimento de campo incorreto',
  'email': 'Endereço de email invalido',
  'comingSoon': 'Em breve',
  'currencySign': 'R$',
  'less': 'menos',
  'more': 'mais',

  // template
  'template.login': 'Sou Aluno',
  'template.contract': 'Contrato de prestação',
  'template.serviceAgreementAndPrivacyPolicy': 'Política de Privacidade e Contrato de Prestação de Serviço',
  'template.privacyPolicy': 'Politica de privacidade',
  'template.phone': 'Telefone',
  'template.aboutUs': 'Sobre nós',
  'template.careersCenter': 'Centro de carreiras',
  'template.jobs': 'Trabalhe conosco',
  'template.b2b': 'Para empresas',
  'template.design': 'Design',
  'template.audioVisual': 'Audiovisual',
  'template.marketing': 'Marketing',
  'template.scheduleAndDate': 'Programação & Data',
  'template.games': 'Games',

  // additionalNavigation component
  'additionalNavigation.careerCenterStudents': 'Para alunos',
  'additionalNavigation.careerCenterCompany': 'Para empresas',

  // firstSection component
  'firstSection.aboutUs': 'Sobre',
  'firstSection.blog': 'Blog',

  // mainNavigation component
  'mainNavigation.aboutUs': 'Sobre nós',
  'mainNavigation.careerCenter': 'Centro de carreiras',
  'mainNavigation.b2b': 'Para empresas',
  'mainNavigation.jobs': 'Trabalhe conosco',

  // phone component
  'phone.error': 'Digite seu número de telefone',

  // reviews component
  'reviews.h2': 'Avaliação dos alunos',

  // plan component
  'plan.h2': 'A rotina dos cursos',
  'plan.1.h3': 'Aprenda sobre o tema',
  'plan.1.description': 'Cursos compostos por videoaulas detalhadas e de fácil entendimento',
  'plan.2.h3': 'Coloque o aprendizado em prática',
  'plan.2.description': 'Atividades para praticar no ritmo mais apropriado para você',
  'plan.3.h3': 'Troque&nbsp;conhecimento com o tutor',
  'plan.3.description': 'Correção individual dos trabalhos e feedback para melhorias',
  'plan.4.h3': 'Apresente um trabalho autoral',
  'plan.4.description': 'Excelente oportunidade para enriquecer seu portfólio',

  // aboutUs component
  'about-us.description': 'A Escola Britânica de Artes Criativas e Tecnologia (EBAC) é uma instituição inovadora de ensino superior em Artes Criativas e Tecnologia que oferece cursos online, além de programas presenciais e híbridos de graduação, especialização e iniciação.',

  'about-us.1.title': '06',
  'about-us.1.description': 'graduações internacionais validadas pela Universidade de Hertfordshire (UK)',

  'about-us.2.title': '+50000',
  'about-us.2.description': 'alunos estudando com nossos cursos online',

  'about-us.3.title': '100%',
  'about-us.3.description': 'dos professores e coordenadores são profissionais atuantes no mercado',

  'about-us.4.title': '+40',
  'about-us.4.description': 'empresas parceiras colaboram no desenvolvimento dos cursos, projetos reais e programas de estágio',

  // promoCode component
  'promoCode.exception': 'Aconteceu um erro, entre em contato com support@innervate.ru',
  'promoCode.couponNotFound': 'O cupom não foi encontrado',
  'promoCode.couponUsed': 'O limite de uso deste cupom acabou',
  'promoCode.couponCannotBeUsedForProduct': 'Este cupom não é válido para o curso "{name}"',
  'promoCode.couponIsNotActive': 'Este cupom não está ativo',
  'promoCode.couponDidNotStart': 'Este cupom não é válido, pois seu prazo de uso tem início em {date}',
  'promoCode.couponExpired': 'Este cupom não pode ser usado porque sua validade expirou em {date}',
  'promoCode.smallDiscount': 'O cupom aplicado tem desconto menor que o atual do curso',
  'promoCode.discount': 'O desconto do cupom aplicado é de {discount}%',
  'promoCode.discountAmount': 'O desconto do cupom aplicado é de {discountAmount}',
  'promoCode.freeCourse': '| O cupom aplicado oferece um curso gratuito de {freeCourse} | O cupom aplicado oferece uns cursos gratuitos de {freeCourse}',

  // partnerSlider component
  'partnerSlider.h2': 'Nossos Parceiros',
  'partnerSlider.text': 'Uma série de acordos entre a EBAC e parceiros da iniciativa pública e privada permitem criar programas educacionais baseados nas demandas atuais e futuras do mercado, proporcionando aos alunos experiência de trabalho com projetos reais.',

  // recommend component
  'recommend.title': 'Você também pode gostar',

  // mainSearch component
  'mainSearch.placeholder': 'Busca',
  'mainSearch.search': 'Search',
  'mainSearch.clear': 'Clear search',

  // filtersContent component
  'filtersContent.filter': 'Filtro',
  'filtersContent.courseType': 'Tipo de curso',
  'filtersContent.courseType.profession': 'Profissão',
  'filtersContent.courseType.profession.text': 'Programa de longa duração com formação abrangente que proporciona um domínio de todos os aspectos da área de atuação escolhida. Você também poderá construir seu portfólio, criar seu currículo e preparar-se para garantir uma colocação no mercado de trabalho.',
  'filtersContent.courseType.course': 'Curso',
  'filtersContent.courseType.course.text': 'Programa de curta duração que permitirá a sua entrada numa especialização na área de atuação escolhida aprendendo habilidades específicas do nível básico ao avançado que você irá usar em seu portfólio ou crescimento profissional.',
  'filtersContent.duration': 'Duração',
  'filtersContent.duration.from': 'De',
  'filtersContent.duration.to': 'a',
  'filtersContent.duration.months': 'meses',
  'filtersContent.apply': 'Aplicar',

  // TODO Pt
  'topicsFilter.title': 'Tópicos',
  'topicsFilter.more': '{count} outros tópicos',
  'topicsFilter.less': 'Colapso',
  'topicsFilter.notFound': 'Este tópico não funciona na direcção {direction} :(',
  'topicsFilter.search': 'Pesquise por tópicos',
  'topicsFilter.notFound': `Sua consulta por "{search}" não encontrou resultados`,

  // saleBanner component
  'saleBanner.mobileHeading': 'Dia do Consumidor. Até',
  'saleBanner.desktopHeading': 'Dia do Consumidor',
  'saleBanner.desktopSubHeading': '',
  'saleBanner.discountTitle': 'Desconto até',
  'saleBanner.button.label': 'ESCOLHA SEU CURSO',
  'saleBanner.mobileHeading.percentAdditional': 'OFF',
  'saleBanner.percent': '60%',

  'saleBanner.eternal.mobileHeading': 'Invista em você. Até',
  'saleBanner.eternal.desktopHeading': 'Invista em você',
  'saleBanner.eternal.desktopSubHeading': '',
  'saleBanner.eternal.discountTitle': 'Desconto até',
  'saleBanner.eternal.button.label': 'ESCOLHA SEU CURSO',
  'saleBanner.eternal.percent': '60%',

  'saleBanner.landing.button.label': 'INSCREVA-SE',

  ambassadorsPage: {
    ambassadorsSection: {
      sectionTitle: 'Nossos Embaixadores'
    }
  },

  // basket component
  'basket.mobileHeader': 'Cursos escolhidos',

  // index page
  'index.main-hero-section.h1': 'APRENDIZADO NA PRÁTICA: SAIA PRONTO PARA TRABALHAR',
  'index.main-hero-section.h2': 'Os melhores cursos que farão você decolar em sua carreira',
  'index.main-hero-section.directions.title': 'Veja nossas áreas:',

  'index.main-courses-section.h2': 'Cursos Online',
  'index.main-courses-section--new.h2': 'Os nossos novos cursos',
  'index.main-courses-section.subtitle': 'Programas com conteúdo desenvolvido no formato de videoaulas, ideais para quem deseja se destacar no mercado de trabalho e quer aprender novas habilidades para evoluir profissionalmente.',
  'index.main-courses-section--new.subtitle': '',
  'index.main-courses-section.btn': 'VER todos OS cursos',
  'index.main-courses-section.profissao': 'Profissão',
  'index.main-courses-section.curso': 'Curso',

  'index.main-webinars-section.h2': 'Webinars Gratuitos',
  'index.main-webinars-section.subtitle': 'Grandes profissionais discutem temas relevantes e seus impactos no mercado de trabalho em transmissões ao vivo.',
  'index.main-webinars-section.btn': 'VER todos OS Webinars',

  'index.main-blog-section.h2': 'Blog',
  'index.main-blog-section.btn': 'VER todos OS posts',

  'index.main-reviews-section.h2': 'Avaliação dos alunos',

  'index.main-plan-section.h2': 'Como é a rotina dos cursos',

  'index.main-plan-section.1.h3': 'Aprenda sobre o tema',
  'index.main-plan-section.1.description': 'Cursos compostos por videoaulas detalhadas e de fácil entendimento',
  'index.main-plan-section.2.h3': 'Coloque o aprendizado em prática',
  'index.main-plan-section.2.description': 'Atividades para praticar no ritmo mais apropriado para você',
  'index.main-plan-section.3.h3': 'Troque&nbsp;conhecimento com o tutor',
  'index.main-plan-section.3.description': 'Correção individual dos trabalhos e feedback para melhorias',
  'index.main-plan-section.4.h3': 'Apresente um trabalho autoral',
  'index.main-plan-section.4.description': 'Excelente oportunidade para enriquecer seu portfólio',

  'index.main-info-section.description': 'A Escola Britânica de Artes Criativas e Tecnologia (EBAC) é uma instituição inovadora de ensino superior em Artes Criativas e Tecnologia que oferece cursos online, além de programas presenciais e híbridos de graduação, especialização e iniciação.',

  'index.main-info-section.1.title': '06',
  'index.main-info-section.1.description': 'graduações internacionais validadas pela Universidade de Hertfordshire (UK)',

  'index.main-info-section.2.title': '+50000',
  'index.main-info-section.2.description': 'alunos estudando com nossos cursos online',

  'index.main-info-section.3.title': '100%',
  'index.main-info-section.3.description': 'dos professores e coordenadores são profissionais atuantes no mercado',

  'index.main-info-section.4.title': '+40',
  'index.main-info-section.4.description': 'empresas parceiras colaboram no desenvolvimento dos cursos, projetos reais e programas de estágio',

  'index.main-form-section.h2': 'Faça seu cadastro',
  'index.main-form-section.description': 'Cadastre-se no nosso mailing e receba, em primeira mão, novidades sobre os cursos online, convites para eventos e webinars gratuitos.',
  'index.main-form-section.name.label': 'Nome',
  'index.main-form-section.name.error': 'Digite seu nome',
  'index.main-form-section.email.label': 'mail@example.com',
  'index.main-form-section.email.error': 'Digite seu e-mail',
  'index.main-form-section.privacy-policy.part1': 'Clicando em enviar você aceita os termos e condições da',
  'index.main-form-section.privacy-policy.part2': 'Política de Privacidade',
  'index.main-form-section.btn': 'Enviar',

  'index.success-dialog.title': 'Obrigado!',
  'index.success-dialog.description': 'Agradecemos seu cadastro. Não deixe de conferir sua caixa de spam.',
  'index.success-dialog.btn': 'Ok',

  'index.upsell-dialog.h3': 'Solicite o contato de um consultor e receba condições especiais para se matricular agora mesmo!',
  'index.upsell-dialog.name.label': 'Nome',
  'index.upsell-dialog.name.error': 'Digite seu nome',
  'index.upsell-dialog.btn': 'SOLICITAR CONTATO',

  // about-us page
  'about-us.title': 'Sobre nós',
  'about-us.description-part-1': 'A Escola Britânica de Artes Criativas & Tecnologia (EBAC) é uma instituição de ensino inovadora que oferece cursos online, além de programas presenciais e híbridos de graduação e especialização. Com um corpo docente formado por excelentes profissionais atuantes no mercado do Brasil e do exterior, nossa escola oferece um novo modelo de ensino das disciplinas que envolvem criatividade, tecnologia, marketing, audiovisual e negócios, além de uma série de iniciativas que preparam o aluno para sua inserção no mercado de trabalho.',
  'about-us.description-part-2': 'A EBAC possui parceria com a University of Hertfordshire, uma das instituições de ensino mais renomadas do Reino Unido. Além disso, nossa plataforma de ensino online conta com uma tecnologia única para que nossos alunos aprendam quando e de onde quiserem.',

  'about-us.areas.h2': 'Nossas áreas de ensino',
  'about-us.areas.card-item-1.title': 'Design',
  'about-us.areas.card-item-1.description': 'Elabore projetos em UX/UI, design de interiores, design gráfico, paisagismo, iluminação, entre outras áreas',
  'about-us.areas.card-item-2.title': 'Audiovisual',
  'about-us.areas.card-item-2.description': 'Seja um profissional que atua em sets de filmagem, edição de vídeo e efeitos visuais',
  'about-us.areas.card-item-3.title': 'Marketing',
  'about-us.areas.card-item-3.description': 'Domine conhecimentos do marketing digital e conheça diferentes estratégias online',
  'about-us.areas.card-item-4.title': 'Software',
  'about-us.areas.card-item-4.description': 'Avance na sua carreira com conhecimento técnico em softwares específicos',
  'about-us.areas.card-item-5.title': 'Programação & Data',
  'about-us.areas.card-item-5.description': 'Aprenda linguagens de programação, códigos, algoritmos e gerenciamento de dados',
  'about-us.areas.card-item-6.title': 'Games',
  'about-us.areas.card-item-6.description': 'Gerencie projetos em games, desenvolva jogos, personagens e cenários em 2D e 3D, com interatividade',
  'about-us.areas.card-item-7.title': 'Negócios',
  'about-us.areas.card-item-7.description': 'Implemente uma gestão estratégica e inovadora para garantir o sucesso dos seus projetos nas mais diversas áreas.',

  'about-us.list.item-1.title': 'Aprenda na prática',
  'about-us.list.item-1.description': 'Formação completa através do método “Aprenda Fazendo” reunindo teoria e prática',
  'about-us.list.item-2.title': 'Feedback profissional',
  'about-us.list.item-2.description': 'Receba o acompanhamento necessário e feedback individualizado de nossos tutores',
  'about-us.list.item-3.title': 'Atualização imediata',
  'about-us.list.item-3.description': 'Conteúdo atualizado para que você esteja por dentro das principais tendências do mercado',
  'about-us.list.item-4.title': 'Portfólio<br/>& Certificado',
  'about-us.list.item-4.description': 'Construa seu próprio portfólio com projetos autorais e obtenha seu certificado de conclusão',

  'about-us.reviews.h2': 'Avaliação dos alunos',

  'about-us.places.h2': 'Estudantes de todo<br/> o Brasil e do mundo',
  'about-us.places.list.item-1': 'São Paulo',
  'about-us.places.list.item-2': 'Curitiba',
  'about-us.places.list.item-3': 'Rio de Janeiro',
  'about-us.places.list.item-4': 'Porto Allegre',
  'about-us.places.list.item-5': 'Recife',
  'about-us.places.list.item-6': 'Miami',
  'about-us.places.list.item-7': 'Brasília',
  'about-us.places.list.item-8': 'Porto',
  'about-us.places.list.item-9': 'Salvador',
  'about-us.places.list.item-10': 'Florianópolis',
  'about-us.places.list.item-11': 'Belo Horizonte',
  'about-us.places.list.item-12': 'Belém',
  'about-us.places.list.item-13': 'Los Angeles',
  'about-us.places.list.item-14': 'Lisboa',
  'about-us.places.list.item-15': 'Manaus',
  'about-us.places.list.item-16': 'Natal',

  // career-center-company page
  'career-center-company.h2': 'Centro de carreiras',
  'career-center-company.description': 'A EBAC auxilia sua empresa a expandir a base de candidatos e facilita o recrutamento de profissionais dos mercados criativo e tecnológico gratuitamente. Desta maneira, fortalecemos sua marca empregadora e colaboramos no desenvolvimento de talentos internos. Veja como:',

  'career-center-company.block-1.title': 'Recrutamento',
  'career-center-company.block-1.description': 'Novos alunos concluem nossos cursos semanalmente, criando assim uma plataforma robusta de potenciais candidatos para diversas oportunidades.<br><br>Auxiliamos a sua empresa a encontrar o candidato ideal de acordo com a sua cultura corporativa e potenciais necessidades de negócio.',
  'career-center-company.block-1.list-1.content': 'Encaminhe para a EBAC as vagas disponíveis',
  'career-center-company.block-1.list-2.content': 'Alinhe conosco o perfil desejado e os principais requisitos',
  'career-center-company.block-1.list-3.content': 'Receba currículos e portfólios dos candidatos mais adequados ao perfil de cada oportunidade',

  'career-center-company.block-2.title': 'Tudo isso, de graça!',
  'career-center-company.block-2.description': 'Seu business case será o projeto final de nossos alunos. Assim, eles terão o primeiro contato com a sua empresa para entender o dia a dia como parte do time e também contribuir para o negócio.',
  'career-center-company.block-2.list-1.content': 'Descreva a sua necessidade ou problema a ser compartilhado com os alunos',
  'career-center-company.block-2.list-2.content': 'Eles (os alunos) trabalharão com base em seu caso durante o projeto final',
  'career-center-company.block-2.list-3.content': 'Apresentamos os projetos e alunos com os melhores resultados',

  'career-center-company.accent-block.part1': 'Além de ser um processo totalmente gratuito, a sua empresa pode sair ganhando, investindo na imagem de empregador que possui no mercado.',
  'career-center-company.accent-block.part2': 'Podemos divulgar em nossas redes sociais os resultados e benefícios dessa parceria, além de utilizar nossos webminars frequentes para aumentar a visibilidade e interesse na sua marca.',

  'career-center-company.block-3.title': 'Desenvolvimento de talentos internos',
  'career-center-company.block-3.description': 'Precisa desenvolver talentos internos, implementar programas de estágio e trainee ou promover a requalificação de colaboradores dentro da sua organização? Podemos ajudar a escolher um curso certo para seus colaboradores ou adaptá-lo de acordo com as necessidades específicas de cada área da empresa.',
  'career-center-company.block-3.list-1.content': 'Escolha a área de seu negócio que necessita promover ações de desenvolvimento e promoção de talentos',
  'career-center-company.block-3.list-2.content': 'Descreva suas metas para cada área ou departamento',
  'career-center-company.block-3.list-3.content': 'Promova adequadamente as ações de educação customizadas para as áreas indicadas',

  // career-center-students page
  'career-center-students.h2': 'Centro de carreiras',
  'career-center-students.description.h3': '“Concluí o meu curso e agora?”',
  'career-center-students.description.subtitle': 'A EBAC trabalha para que você alcance seus objetivos!',
  'career-center-students.description.body-m': 'Estamos 100% focados na trajetória de nossos alunos não apenas ao longo de seus estudos, mas também na hora de ingressar no mercado de trabalho. O nosso centro de carreiras é feito por um time de profissionais que trabalha em diversas frentes em prol do futuro de quem estuda com a gente.',
  'career-center-students.step-1.title': 'Recrutadores profissionais auxiliam você a estruturar seu currículo e se preparar para entrevistas',
  'career-center-students.step-1.description': 'Temos uma equipe de especialistas da área de recursos humanos com ampla experiência em recrutamento e seleção em diferentes setores prontos para compartilhar seus conhecimentos.<br><br>Trabalharemos com você os principais temas como a estruturação do seu currículo e perfil no LinkedIn, inseguranças na hora das entrevistas, mapeamento de habilidades para identificar as suas principais forças, entre outros aspectos dos processos seletivos.',
  'career-center-students.step-2.title': 'Seu projeto final realizado em colaboração com nossas empresas parceiras',
  'career-center-students.step-2.description': 'A EBAC está ampliando constantemente sua rede de parcerias. São empresas para as quais os alunos são indicados e onde, ao final de seus projetos de conclusão baseados em cases reais, podem trabalhar. Os estudantes com os melhores projetos são avaliados e podem começar uma nova etapa de suas vidas profissionais já empregados.',
  'career-center-students.step-3.title': 'Seleção das melhores vagas para os nossos alunos',
  'career-center-students.step-3.description': 'Monitoramos em tempo real as vagas disponíveis no mercado de trabalho a fim de apresentar os currículos e portfólios de nossos estudantes aos times de seleção.',
  'career-center-students.step-4.title': 'Mentoria com recrutadores e profissionais das empresas mais sonhadas pelos candidatos',
  'career-center-students.step-4.description': 'Convidamos para os nossos webinars os recrutadores das empresas mais desejadas para se trabalhar. Eles compartilharão os seus segredos: no que mais reparam durante o processo seletivo, tipos de avaliação, planos de carreira e muito mais.<br><br>Por isso, todo o seu foco deve estar voltado para as aulas! Estude, tire suas dúvidas com os tutores, assista aos nossos webinars e faça um excepcional projeto final. Isso já é boa parte do caminho andado!<br><br>O resto você pode deixar com a gente. Nós ajudaremos a sua carreira decolar!',

  // vacancies page
  'vacancies.h2': 'Trabalhe conosco',
  'vacancies.description': 'A EBAC ensina às pessoas as profissões mais modernas e demandadas pelo mercado. Todos nossos esforços são voltados à melhoria da qualidade de ensino e à expansão da abrangência dos nossos cursos. Isto só acontece graças ao nosso time de profissionais dedicados e comprometidos com a ideia de criar uma plataforma de ensino online que te faça querer estudar de verdade.',
  'vacancies.vacanciesListTitle': 'Se você quiser se juntar ao time da EBAC, veja aqui as nossas oportunidade abertas:',
  'vacancies.noVacancies.part1': 'Oops, parece que não temos nenhuma vaga disponível no momento.',
  'vacancies.noVacancies.part2': 'Envie seu currículo e entraremos em contato assim que surgirem novas oportunidades.',
  'vacancies.emailText': 'Se você não identificou nenhuma oportunidade em aberto na sua área de atuação, se apresente para nós por e-mail:',
  'vacancies.email': 'carreer@ebac.art.br',
  'vacancies.directions.0': 'Todas as vagas',

  // sale page
  'sale.h1': 'Nós te amamos muito mesmo!',
  'sale.subtitle.part1': 'Desconto de 50%',
  'sale.subtitle.part2': 'em todos os cursos online. Tá na hora de dar aquele salto na carreira e conquistar seu diferencial no mercado de trabalho. Garanta já seu curso na EBAC Promo Week!',
  'sale.endOfPromotion': 'Fim da Promoção',
  'sale.filter': 'Filtro',
  'sale.noInterestOnCardPart1': 'de',
  'sale.noInterestOnCardPart2': 'sem juros no cartão',
  'sale.installmentInUpToPart1': 'Parcelamento em até',
  'sale.installmentInUpToPart2': 'vezes',
  'sale.installmentInUpToPart3': 'no cartão',
  'sale.placeAnOrder': 'Fechar compra',
  'sale.discountedAmount': 'Valor com desconto',
  'sale.content.section.professions.title': 'Profissões',
  'sale.content.section.courses.title': 'Cursos',
  'sale.product.add': 'Adicionar',
  'sale.product.remove': 'Remover do carrinho',
  'sale.product.moreDetails': 'Mais sobre o curso',
  'sale.content.more': 'Mais',
  'sale.h2': 'A gente ajuda na escolha do melhor curso para você',
  'sale.description': 'Quer ter mais informações sobre os cursos e decidir qual é o melhor de acordo com suas necessidades e expectativas? Preencha seus dados e um de nossos consultores entrará em contato com você.',
  'sale.name.label': 'Nome',
  'sale.name.error': 'Digite seu nome',
  'sale.email.label': 'mail@example.com',
  'sale.email.error': 'Digite seu e-mail',
  'sale.privacy-policy.part1': 'Clicando em enviar você aceita os termos e condições da',
  'sale.privacy-policy.part2': 'Política de Privacidade',
  'sale.btn': 'Cadastrar',
  'sale.detailProductDialog.courseProgram': 'Programa do Curso',
  'sale.detailProductDialog.professionProgram': 'Programa de profissão',
  'sale.detailProductDialog.skillsBlockTitle': 'O que você irá aprender',
  'sale.downloadCourseProgram': 'Baixe o programa do curso para saber mais',
  'sale.trailer': 'Trailer do Curso',
  'sale.detailProductDialog.teachers': 'Professores',
  'sale.detailProductDialog.footer.addButton': 'Adicionar ao carrinho',
  'sale.basketDialogContent.title': 'Seu pedido',
  'sale.basketDialogContent.total': 'Total',
  'sale.basketDialogContent.buttonLabel': 'Comprar',
  'sale.free': 'Grátis',
  'sale.thx.thx': 'Obrigado!',
  'sale.thx.description.part1': 'Agradecemos sua inscrição na EBAC. Em breve, você vai receber um email com mais informações.',
  'sale.thx.description.part2': 'Ah, não deixe de checar a sua caixa de SPAM. Se você quiser finalizar sua matrícula agora e garantir seu desconto desde já, você pode optar pelo pagamento online. O link vai ser exibido em breve.',
  'sale.thx.description.part3': 'Atenção: esta promoção é válida apenas até',
  'sale.thx.description.part4': 'às',
  'sale.thx.loading': 'Carregando...',
  'sale.thx.endEnrollment': 'FINALIZAR MATRÍCULA',

  // courses page
  'courses.professions': 'Profissões',
  'courses.courses': 'Cursos',
  'courses.profession': 'Profissão',
  'courses.course': 'Curso',
  'courses.more': 'Mais',
  'courses.notFound': 'Não foram encontrados resultados para a sua pesquisa',

  // sale event types
  'sale.event_type.webinar': 'DO WEBINAR',
  'sale.event_type.marathon': 'DA MARATONA',
  'sale.event_type.workshop': 'DO WORKSHOP',
  'sale.event_type.discussion': 'DO DICAS DE CARREIRA',
  'sale.event_type.demonstration': 'DA DEMONSTRAÇÃO',
  'sale.event_type.success_case': 'DO CASE DE SUCESSO',
  'sale.event_type.masterclass': 'DA MASTERCLASS',
  'sale.nomenclature_type.0': 'Curso',
  'sale.nomenclature_type.1': 'Profissão',
  'sale.nomenclature_type.0.en': 'Cource',
  'sale.nomenclature_type.1.en': 'Profession',

  'acceptTheTermsAndConditions': 'Eu li e aceito os termos e condições da',
  'privacyPolicyAndServiceAgreement': 'Política de Privacidade e Contrato de Prestação de Serviço',

  'formText.part1': 'Eu li e aceito os termos e condições da',
  'formText.part2': 'Política de Privacidade e Contrato de Prestação de Serviço',

  'webinarSale.badge.title': 'de desconto',
  'webinarSale.first.title': 'VENDA EXCLUSIVA PARA PARTICIPANTES',
  'webinarSale.first.subtitle': 'Promoção por tempo limitado',
  'webinarSale.timer.title': 'Até o fim da promoção:',
  'webinarSale.timer.days': 'days',
  'webinarSale.timer.hours': 'hours',
  'webinarSale.timer.minutes': 'minutes',
  'webinarSale.timer.seconds': 'seconds',
  "webinarSale.second.subtitle": "{value} de desconto!",
  'webinarSale.price.first.title': 'Preço original',
  'webinarSale.price.first.economy': 'Você economiza',
  'webinarSale.price.second.title': 'Preço com desconto',
  'webinarSale.price.second.subtitle': 'sem juros',
  'webinarSale.sendButton': 'Inscreva-se',
  'webinarSale.dialog.title': 'Faça sua inscrição para o curso',
  'webinarSale.dialog.field.name.label': 'Nome',
  'webinarSale.dialog.field.name.error': 'Digite seu nome',
  'webinarSale.dialog.field.email.error': 'Digite seu e-mail',
  'webinarSale.dialog.sendButton': 'INSCREVA-SE',

  'thankYouPage.title': 'Obrigado!',
  'thankYouPage.description.part1': 'Obrigado pela sua inscrição, você receberá um email em breve com mais detalhes.',
  'thankYouPage.description.part2': '(Ah! Não deixe de checar sua caixa de spam)',
  'thankYouPage.description.part3': 'Quer finalizar sua matrícula agora?',
  'thankYouPage.description.part4': 'Faça o pagamento online. É simples, rápido e seguro.',
  'thankYouPage.btn.label': 'FINALIZAR MATRÍCULA',
  'thankYouPage.btn.loading': 'Carregando...',

  'thankYouPage.contract.0': 'Eu li e aceito os termos e condições da',
  'thankYouPage.contract.1': 'Política de Privacidade e Contrato de Prestação de Serviço',

  // Offers
  'offer.title': 'Personal offer!',
  'offer.description': 'To get all the courses at a special price, pay the amount by clicking on the "Next to payment" button. After that, access to your personal account will come to your email. ',
  'offer.orderId': 'Order {id}',
  'offer.courses': 'Courses',
  'offer.payAll': 'Next to Payment',
  'offer.pay': 'Pay',
  'offer.price.courses': 'Courses ({count})',
  'offer.price.discount': 'Discount',
  'offer.price.total': 'Total',

  'offer.paymentProgress': 'Payment Progress',
  'offer.paymentLink.part': 'Part {index}',
  'offer.payment.status.success': 'Paid',
  'offer.paymentSuccess.title': 'Congratulations!',
  'offer.paymentSuccess.text': 'The course is fully paid for. Within five minutes you will receive an email with a username and password to access your personal account.',

  'offer.paymentLinksNotFound': "Payment links not found",

  'webinar.directions.all.eventMetaDescription': 'Os Webinars irão ajudá-lo a dominar as habilidades em design, programação e marketing. Inscreva-se nos eventos e assista às transmissões anteriores.',
  'webinar.directions.all.eventMetaTitle': "Webinars gratuitos e eventos EBAC Online",
  'webinar.directions.all.eventTitle': "Webinars Gratuitos",

  'webinar.search.notFound': 'Não foram encontrados resultados para a sua pesquisa',
  'webinar.search.more': 'Mais eventos',
  'webinar.type.discussion': 'Dicas de carreira',
  'webinar.type.success_case': 'Cases de sucesso',
  'webinar.type.webinar': 'Webinar',
  'webinar.type.workshop': 'Workshop',
  'webinar.type.marathon': 'Maratona',
  'webinar.type.demonstration': 'Demonstração',
  'webinar.type.event': 'Evento',
  'webinar.type.masterclass': 'Masterclass',

  'webinar.chat.title': 'Chat',
  'webinar.chat.register': 'Participe da discussão',
  'webinar.chat.close': 'Ocultar chat',
  'webinar.chat.open': 'Abrir chat',

  'webinar.schedule.title': 'Programação por dia',
  'webinar.speaker.title': 'Palestrante',

  'webinar.topic.status.upcoming': 'Em breve',
  'webinar.topic.status.live': 'Ao Vivo',
  'webinar.topic.status.passed': 'Transmitido',

  'webinar.preRegister.title': 'Para acessar o evento, use o link de qualquer um dos e-mails que você recebeu após sua inscrição',
  'webinar.preRegister.description': 'Se você ainda não se inscreveu, agora é a hora!',
  'webinar.preRegister.submit': 'Inscreva-se',

  'webinar.register.title': 'Preencha com os seus dados.<br/>Enviaremos um link de&nbsp;participação e lembretes por&nbsp;email',
  'webinar.register.label.name': 'Nome',
  'webinar.register.error.name': 'Digite seu nome',
  'webinar.register.label.email': 'mail@example.com',
  'webinar.register.error.email': 'Digite seu e-mail',
  'webinar.register.submit': 'INSCREVA-SE',

  'webinar.registered': 'Você está inscrito',

  'webinar.register.thanks.title': 'Obrigada por se inscrever!',
  'webinar.register.thanks.text': 'Você receberá o&nbsp;email de&nbsp;confirmação com o&nbsp;link de&nbsp;participação em até cinco minutos. Para participar do&nbsp;evento, você pode usar o&nbsp;link de&nbsp;qualquer email que você tenha recebido depois do&nbsp;cadastro. Se você não recebeu o&nbsp;email de&nbsp;confirmação, por&nbsp;favor, cheque a&nbsp;sua pasta de&nbsp;SPAM.',
  'webinar.register.thanks.ok': 'OK',

  'webinar.opinion.title': 'Compartilhe a sua opinião!',
  'webinar.opinion.label.text': 'Envie seu feedback sobre o evento e ajude-nos a melhorar ainda mais',
  'webinar.opinion.error.text': 'Digite seu comentário',
  'webinar.opinion.label.email': 'E-mail para contato',
  'webinar.opinion.error.email': 'Digite seu e-mail',
  'webinar.opinion.privacy': 'Clicando em enviar você aceita os termos e condições da',
  'webinar.opinion.privacy.link': 'Política de Privacidade',
  'webinar.opinion.submit': 'Enviar',

  'webinar.other.title': 'Outros Webinars',

  'webinar.promo.register.title': 'Cadastre-se para participar do evento',
  'webinar.promo.register.submit': 'Inscreva-se',
  'webinar.promo.discount': 'Aproveitaram o desconto',

  'webinar.recommends': 'Recomendados',

  'webinar.filter.status.all': 'Todos',
  'webinar.filter.status.upcoming': 'Em breve',
  'webinar.filter.status.finished': 'Anteriores',

  // Blog
  'blog.directions.all.metaDescription': "O Blog da EBAC sobre design, marketing, software, programação & data, vídeo, áudio e jogos. Inspire-se com conteúdo especializado para profissionais e iniciantes.",
  'blog.directions.all.metaTitle': "EBAC | Blog",
  'blog.directions.all.seoText': "O Blog da EBAC sobre design, marketing, software, programação & data, vídeo, áudio e jogos. Inspire-se com conteúdo especializado para profissionais e iniciantes.",
  'blog.directions.all.title': "Blog",

  //Contact Information
  'contactInformation.title': 'Contact Information',
  'contactInformation.titlePersonal': 'Personal',
  'contactInformation.titleContact': 'Contact',
  'contactInformation.titleAddress': 'Address',
  'contactInformation.fullName.label': 'Nome completo',
  'contactInformation.fullName.error': 'Digite seu Nome completo',
  'contactInformation.data.label': 'Data de nascimento',
  'contactInformation.data.error': 'Digite seu Data de nascimento',
  'contactInformation.email.label': 'E-mail',
  'contactInformation.email.error': 'Digite seu E-mail',
  'contactInformation.confirmEmail.label': 'Confirme seu E-mail',
  'contactInformation.confirmEmail.error': 'Digite seu E-mail',
  'contactInformation.confirmEmail.confirmError': 'Digite seu Confirme seu E-mail',
  'contactInformation.zipCode.error': 'Digite seu zip Code',
  'contactInformation.address.label': 'Endereço',
  'contactInformation.address.error': 'Digite seu Endereço',
  'contactInformation.number.label': 'Número',
  'contactInformation.number.error': 'Digite seu Número',
  'contactInformation.complement.label': 'Complemento',
  'contactInformation.complement.error': 'Digite seu Complemento',
  'contactInformation.city.label': 'Cidade',
  'contactInformation.city.error': 'Digite seu Cidade',
  'contactInformation.neighborhood.label': 'Bairro',
  'contactInformation.neighborhood.error': 'Digite seu Bairro',
  'contactInformation.cpf.error': 'Digite seu Bairro',
  'contactInformation.part1': 'Eu li e aceito os termos e condições',
  'contactInformation.part2': 'da Política de Privacidade e Contrato de Prestação de Serviço',
  'contactInformation.btn': 'Enviar',

  //page 404
  'page404.title': "Oops!",
  'page404.subtitle': "Lo lamentamos, esta pagina no existe",
  'page404.text.part1': "Tente ir para a",
  'page404.text.link1': "página inicial",
  'page404.text.part2': "e começar de novo. Ou conheça nossos",
  'page404.text.link2': "cursos",
  'page404.text.part3': ",",
  'page404.text.link3': "webinars",
  'page404.text.part4': "e",
  'page404.text.link4': "blog",
}
