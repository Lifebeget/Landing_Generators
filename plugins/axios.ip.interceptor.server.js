export default function ({ $axios,  req }) {
  $axios.interceptors.request.use(function (config) {
    config.headers['x-user-ip'] = req.userIp;
    return config;
  });
}
