import { extend, setInteractionMode } from "vee-validate";
import { required, min, email } from "vee-validate/dist/rules";

export default async ({ app }) => {
  extend("required", {
    ...required,
    message: app.i18n.t('required')
  });
  extend("min", {
    ...min,
    message: app.i18n.t('min')
  });
  extend("email", {
    ...email,
    message: app.i18n.t('email')
  });

  setInteractionMode('custom', ({ errors, flags}) => {
    if (flags.validated) {
      return {
        on: ['input', 'change']
      };
    }

    return {};
  });

}
