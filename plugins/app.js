import Vue from 'vue';
import cookie from 'js-cookie';
import getCookie from '../utils/getCookie';
import config from '../config';

const sbjs = require('sourcebuster');

export default async ({ app, error, $config, req }, inject) => {
  let user = null;
  const cookies = process.browser ? document.cookie : req.headers.cookie;
  if (cookies) {
    const accessToken = getCookie({ name: 'a_token', cookies });
    if (accessToken) {
      try {
        const response = await app.$axios({
          method: 'get',
          url: `${$config.LMS_REST_API}/auth/info`,
          headers: {
            'Authorization': `Bearer ${accessToken}`,
          },
        });

        if (response.data) {
          user = response.data;
        }
      } catch (error) {
        console.error(error);
      }
    }
  }

  app.store.dispatch("topics/getTopics");

  const directions = await getDirections({ axios: app.$axios, error, $config }) || [];
  // Save raw directions to webinars store
  const webinarsDirections = [
    {
      active: true,
      id: null,
      eventMetaDescription: app.i18n.t('webinar.directions.all.eventMetaDescription'),
      eventMetaKeywords: null,
      eventMetaTitle: app.i18n.t('webinar.directions.all.eventMetaTitle'),
      eventSeoText: null,
      eventSlug: null,
      eventTitle: app.i18n.t('webinar.directions.all.eventTitle'),
    },
    ...directions
  ]
  app.store.commit('webinars/setDirections', Object.assign([], webinarsDirections));
  app.store.commit('courses/setDirections', Object.assign([], [
    {
      id: null,
      metaKeywords: '',
      metaDescription: app.i18n.t('direction.0.metaDescription'),
      metaTitle: app.i18n.t('direction.0.metaTitle'),
      seoText: app.i18n.t('direction.0.seoText'),
      title: app.i18n.t('direction.0.title'),
      slug: 'cursos',
    },
    ...directions,
  ]));
  app.store.commit('blog/setDirections', Object.assign([], [
    {
      id: null,
      metaKeywords: '',
      metaDescription: app.i18n.t('blog.directions.all.metaDescription'),
      metaTitle: app.i18n.t('blog.directions.all.metaTitle'),
      seoText: app.i18n.t('blog.directions.all.seoText'),
      title: app.i18n.t('blog.directions.all.title'),
      slug: 'all',
      shortName: 'All',
    },
    ...directions,
  ]));

  const conf = config($config.COUNTRY);
  inject('app', new Vue({
    i18n: app.i18n,
    data() {
      return {
        isMobile: false,
        breakPoint: 1280,
        backToTopButtonBottomOffset: 0,

        user,
        config: conf,
        cookies: {
          lastScheduleEvent: null,
        },

        ui: {
          webinars: {
            isDisplayPromoBlockRegistration: false,
          },
        },
      }
    },
    created() {
      if (process.browser) {
        this.sbjs = sbjs;
        if (typeof sbjs === "object" && typeof sbjs.init === "function") {
          sbjs.init();
        } else {
          console.info(`no sbjs`);
        }

        if (conf.current?.bodyScripts.length) {
          for (let script of conf.current.bodyScripts) {
            this.addScriptToBody(script);
          }
        }
      }

      setTimeout(() => {
        this.cookies.lastScheduleEvent = this.getCookie('last_schedule_event');
      }, 0);
    },
    methods: {
      getCurrency(currency) {
        const currencyNumber = Number(currency);
        if ((currencyNumber ^ 0) === currencyNumber) {
          return this.$n(currencyNumber, 'currencyNoCents');
        } else {
          return this.$n(currencyNumber, 'currency');
        }
      },
      getCookie(cookieName) {
        return cookie.get(cookieName);
      },
      addScriptToBody(script){
        const scriptNode = document.createElement('script');
        scriptNode.src = script.src;
        if (script.hasOwnProperty('type')) scriptNode.type = script.type;
        if (script.hasOwnProperty('async')) scriptNode.async = script.async;
        if (script.hasOwnProperty('defer')) scriptNode.defer = script.defer;
        document.body.appendChild(scriptNode);
      }
    },
    watch: {
      'ui.webinars.isDisplayPromoBlockRegistration'() {
        if (process.client){
          window.dispatchEvent(new Event('recalculateCookieConsent'));
        }
      },
    },
  }));
}

const getDirections = async ({ axios, error, $config }) => {
  try {
    const response = await axios({
      method: 'post',
      url: `${$config.CMS_REST_API}/public/directions/`,
    });

    if (response.data.error) throw new Error(`An error occurred while executing the query (${response.data.error})`);
    const data = response.data.data;

    return data.data;
  } catch (e) {
    console.error(`error`, e);
    // error({ statusCode: 500, message: 'Os trabalhos técnicos estão em andamento' });

    return null;
  }
};

