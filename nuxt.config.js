import colors from 'vuetify/es5/util/colors'
import config from './config'
import pageGuard from './middleware/pageGuard'

const isDev = (process.env.NODE_ENV !== 'production');
const COUNTRY = process.env.COUNTRY || 'br';
const conf = config(COUNTRY);

export default {
  publicRuntimeConfig: {
    /**
     * Url-адрес до сервера CMS
     * */
    CMS_REST_API: process.env.CMS_REST_API || 'http://localhost:3000',
    /**
     * Url-адрес до сервера billing
     * */
     BILLING_REST_API: process.env.BILLING_REST_API || 'http://localhost:3002',
    /**
     * Url-адрес до сервера LMS
     * */
    LMS_REST_API: process.env.LMS_REST_API || 'https://api.lms.ebaconline.com.br',
    /**
     * После успешной отправки формы появляется всплывающее окно, константа контролирует время, через которое
     * всплывающее окно автоматически закроется
     * */
    CLOSE_SUCCESS_DIALOG_TIME: process.env.CLOSE_SUCCESS_DIALOG_TIME || 5000,
    /**
     * Время перед стартом трансляции, при котором трансляция считается начавшейся
     * */
    DISPLAY_LIVE_WEBINAR_CHAT_FOR: process.env.DISPLAY_LIVE_WEBINAR_CHAT_FOR || 1000 * 60 * 10,
    /**
     * Время через котрое трансляция считается заверженной
     * */
    DISPLAY_READONLY_WEBINAR_CHAT_AFTER: process.env.DISPLAY_READONLY_WEBINAR_CHAT_AFTER || 1000 * 60 * 60 * 4,
    /**
     * Время через котрое будет перезагружен emby чат (!чат перезагружается, если emby перестает отправлять
     * событие)
     * */
    EMBY_TERMINATOR_TIMEOUT_DELAY: 1000 * 30,
    /**
     * Тип клиента (бразильский, мексиканский, ...)
     * */
    COUNTRY,
    GETSTREAM_APIKEY: process.env.GETSTREAM_APIKEY || 'nqkbqz89cmg4',
    GETSTREAM_URL: process.env.GETSTREAM_URL || 'http://localhost:3002/webchat',

    featureToggle:{
      queryString: true,
      toggles: {}
    }
  },
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    ...conf.current?.head,
    htmlAttrs: {
      lang: conf.current?.head?.lang,
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0' },
      { 'http-equiv': 'X-UA-Compatible', content: 'ie=edge' },
      ...conf.current?.head?.meta,
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      ...conf.current?.head?.link,
    ],
    script: [
      ...conf.current?.head?.script,
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    './style/main.scss',
    './style/reset.sass',
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/axios.ip.interceptor.server.js', mode: 'server' },
    { src: '~/plugins/app.js' },
    '~/plugins/vee-validate.js',
    { src: '~/plugins/vue-youtube-embed.js', ssr: false },
    { src: '~/plugins/v-mask.js', ssr: false },
    { src: '~/plugins/date-picker.js', ssr: false }
  ],

  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['font'].includes(type)
      }
    }
  },

  router: {
    prefetchLinks: false,
    middleware: ['pageGuard', 'detectLandingPage'],
    async extendRoutes(routes, resolve) {
      routes.push({
        name: 'webinarSale',
        path: '/webinars/:eventSlug/:saleSlug',
        component: './pages/-webinarSale',
      })

      routes.push({
        name: 'webinarSaleThanks',
        path: '/webinars/:eventSlug/:saleSlug/thankyou',
        component: './pages/-webinarSaleThanks',
      })

      routes.push({
        name: 'coursesTopic',
        path: '/cursos/:topic',
        component: './pages/-coursesTopic',
      })
    },
  },

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/svg',
    '@nuxtjs/dayjs',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // '~/vendor/gtm/lib/module.js',
    '@nuxtjs/gtm',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    [
      'nuxt-i18n',
      {
        locales: conf.common.locales,
        defaultLocale: conf.current.localeName,
        vueI18n: {
          fallbackLocale: conf.current.localeName,
          messages: conf.common.messages,
          numberFormats: conf.common.numberFormat,
        },
        detectBrowserLanguage: false,
      },
    ],
    ['vue-scrollto/nuxt', { duration: 300 }],
    ['nuxt-lazy-load', {
      // These are the default values
      images: true,
      // videos: true,
      // audios: true,
      // iframes: true,
      native: false,
      polyfill: true,
      directiveOnly: false,

      // To remove class set value to false
      loadingClass: 'isLoading',
      loadedClass: 'isLoaded',
      appendClass: 'lazyLoad',
    }],
    ['@nuxtjs/router', { keepDefaultRouter: true }],
    '@nuxt/content',
    '@nuxtjs/sentry',
    'nuxt-feature-toggle'
  ],
  sentry: {
    dsn: 'https://9fcf35936f0e4c01aa2b8943c0aa17c0@o934872.ingest.sentry.io/6217096',
    // Additional Module Options go here
    // https://sentry.nuxtjs.org/sentry/options
    tracesSampleRate: 0.5,
    vueOptions: {
      tracing: true,
      tracingOptions: {
        hooks: [ 'mount', 'update' ],
        timeout: 2000,
        trackComponents: true
      }
    },
    config: {
      // Add native Sentry config here
      // https://docs.sentry.io/platforms/javascript/guides/vue/configuration/options/
    },
  },

  privateRuntimeConfig: {},
  serverMiddleware: ['~/server-middleware/ip-catcher'],
  loading: false,
  ...!isDev && { modern: 'server' },
  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,
  gtm: conf.current.gtm,
  dayjs: {
    locales: [conf.current.localeName],
    plugins: [
      'utc',
      'duration',
      'localizedFormat',
    ]
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/style/vuetify-variables.scss'],
    theme: {
      themes: {
        light: {
          primary: '#D36133',
          secondary: '#905DB4',
          negative: '#EB5757',
          accent: colors.shades.black,
          error: colors.red.accent3,
        },
        dark: {
          primary: colors.blue.lighten3,
        },
      },
    },
    defaultAssets: false,
    treeShake: true,
  },
  vue: {
    html: {
      query: {attrs: "img:src img:srcset"}
    }
  },
  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},
  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    analyze: false,
    filenames: {
      app: ({ isDev }) => isDev ? '[name].js' : '[name].[contenthash].js',
      chunk: ({ isDev }) => isDev ? '[name].js' : '[name].[contenthash].js',
      css: ({ isDev }) => isDev ? '[name].css' : '[name].[contenthash].css',
    },
    devtools: isDev,
    hardSource: false,
    parallel: true,
    terser: true,
    html: {
      minify: {
        collapseBooleanAttributes: true,
        decodeEntities: true,
        minifyCSS: true,
        minifyJS: true,
        processConditionalComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        trimCustomFragments: true,
        useShortDoctype: true
      }
    },
    splitChunks: {
      layouts: false,
      pages: true,
      commons: true
    },
    optimizeCSS: !isDev,
    transpile: [
      'vee-validate/dist/rules',
      'vuetify/lib',
    ],
    // extractCSS: !isDev,
    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        chunks: 'all',
        maxInitialRequests: Infinity,
        minSize: 0,
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name(module) {
              const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
              return `npm.${ packageName.replace('@', '') }`;
            },
          },
        },
      },
    },
  },
  server: {
    port: Number(process.env.NUXT_PORT) || 3001, // default: 3000
    host: process.env.NUXT_HOST || 'localhost' // default: localhost
  },
  static: {
    prefix: false
  },
}
