export default function ({ route, error, store }) {
  error({ statusCode: 404, message: 'This page could not be found' });
}
