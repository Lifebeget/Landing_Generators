export default function ({ error, store, redirect }) {
  if (!store.state.sale.sale) return error({ statusCode: 404, message: 'This page could not be found' });
  if (new Date(store.state.sale.sale.endDate).getTime() < new Date().getTime()) return redirect('/');
}
