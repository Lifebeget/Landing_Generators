export default async function ({ app, route, error, store, $config }) {
  if (route.query.sale) {
    try {
      const response = await app.$axios({
        method: 'post',
        url: `${$config.CMS_REST_API}/public/sale`,
        data: {
          slug: route.query.sale,
        },
      });

      if (response?.data?.data) {
        store.commit('thankYouSale/setSale', response.data.data);
      }
    } catch (error) {
      console.error(error);
    }
  }
}
