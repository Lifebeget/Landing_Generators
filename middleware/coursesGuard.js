export default function ({ route, error, store }) {
  const courses = store.state.courses.directions;
  const course = route.params.courses;
  if (!courses.find(_course => _course.slug === course)) error({ statusCode: 404, message: 'This page could not be found' });
}
