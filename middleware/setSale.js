export default async function ({ app, route, error, store, $config }) {
  if (!store.state.sale.sale) {
    try {
      const response = await app.$axios({
        method: 'post',
        url: `${$config.CMS_REST_API}/public/sale`,
        data: {
          slug: route.params.sale,
        },
      });

      if (response?.data?.data) {
        store.commit('sale/setSale', response.data.data);
      }
    } catch (error) {
      console.error(error);
    }
  }
}
