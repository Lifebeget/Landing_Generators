# Cookies y Beacons
EBAC  EDUCACIÓN,  S.A.  DE  C.V.,  en  adelante  “EBAC”  te  informa  que  este  sitio  web,  utiliza cookies,  web  Beacons  y  otras  tecnologías  a  través  de  las  cuales  es  posible  monitorear  el comportamiento  del  titular  como  usuario  de  Internet,  con  la  finalidad  de  brindar  un  mejor servicio y/o experiencia al navegar en el mismo.

Los Datos Personales que se podrán obtener, de manera enunciativa más no limitativa, son: (i) datos  generales;  (ii)  datos  de  contacto;  (iii)  horario  y  tiempo  de  navegación;  (iv)  sitios  web  y secciones consultadas; (v) ubicación; de conformidad con las políticas de EBAC.
