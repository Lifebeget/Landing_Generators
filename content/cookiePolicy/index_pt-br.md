# ebaconline.com.br Política de Cookies
Por favor, leia cuidadosamente esta política de cookies, pois ela contém informações importantes sobre quem somos e como usamos cookies em nosso website. Esta política estabelece como e porque coletamos, armazenamos, usamos e compartilhamos informações pessoais em geral, bem como seus direitos em relação a suas informações pessoais e detalhes de como entrar em contato conosco e com as autoridades supervisoras caso tenha alguma reclamação.

## Quem somos?
Esse site é operado pela Escola Britânica de Artes Criativas & Tecnologia (EBAC)

## Cookies
Um cookie é um pequeno arquivo de texto que nossos sites armazenam em seu computador ou dispositivo móvel quando você visita nosso site. Nosso website envia estes dados para seu navegador quando você solicita uma página pela primeira vez e depois armazena os dados em seu computador ou outro dispositivo para que o website ou aplicativo possa acessar, armazenar ou coletar informações de seu dispositivo quando você solicita uma página da web pela primeira vez. Os navegadores suportam cookies e tecnologias similares (como armazenamento local e pixels) para que nossos websites possam se lembrar de informações sobre sua visita e possam usar as informações para melhorar sua experiência e para criar estatísticas anônimas agregadas sobre o uso do site. Nesta Política, usamos o termo "cookie" para nos referirmos tanto a cookies quanto a tecnologias similares.

Os cookies podem ser definidos pelo site que você está visitando ("cookies primários") ou por terceiros, tais como aqueles que fornecem serviços de análise, publicidade ou conteúdo interativo no site ("cookies de terceiros"). Além de usar cookies em nossos sites, podemos também servir nossos cookies (especificamente, nosso pixel publicitário) em sites de terceiros operados por nossos anunciantes que usam nossa plataforma de publicidade.


### Cookies de terceiros

Nossos serviços também incluem cookies de terceiros com os quais temos parcerias ou que nos prestam serviços.

Para maiores informações sobre cookies em geral, incluindo como controlá-los e gerenciá-los, visite as orientações sobre cookies publicadas pelo UK Information Commissioner's Office, https://www.aboutcookies.org ou https://www.allaboutcookies.org.

## Categorias de Cookies

### Estritamente necessários

Estes cookies são necessários para que nosso website funcione corretamente e não podem ser desativados em nossos sistemas. Eles são normalmente definidos apenas em resposta a ações feitas por você, correspondendo a uma solicitação de serviços, tais como definir suas preferências de privacidade, fazer login, preencher formulários ou onde eles são essenciais para lhe fornecer um serviço solicitado. Você não pode optar por não aceitar estes cookies. Você pode configurar seu navegador para bloquear ou alertá-lo sobre esses cookies, mas se o fizer, algumas partes do site não funcionarão. Estes cookies não armazenam nenhuma informação de identificação pessoal.


Domínio | Cookies | Utilizado como
--- | --- | ---
ebaconline.com.br | last_schedule_event, sale-banner-<date>, sale-banner_eternal, a_token, sbjs_session, sbjs_udata, sbjs_first_add, sbjs_current, sbjs_first, sbjs_migrations, sbjs_current_add, ebac_consent | Primários
youtube.com | __Secure-3PSIDCC, SIDCC, PREF, LOGIN_INFO, APISID, HSID, __Secure-1PSID, YSC, SSID, __Secure-3PSID, VISITOR_INFO1_LIVE, __Secure-3PAPISID, SID, SAPISID, __Secure-1PAPISID,  | De terceiros

### Cookies de Marketing

Estes cookies nos permitem contar visitas e fontes de tráfego para que possamos medir e melhorar o desempenho de nosso site. Eles nos ajudam a saber quais páginas são as mais e menos populares e ver como os visitantes se movimentam pelo site, o que nos ajuda a otimizar sua experiência. Todas as informações que esses cookies coletam são agregadas e, portanto, anônimas. Se você não permitir estes cookies, não poderemos utilizar seus dados desta forma.


Domínio | Cookies | Utilizado como
--- | --- | ---
ebaconline.com.br | _ga, _gid, _gat_<container-id>, _gac_gb_<container-id>, activity, blueID, _clck, _pin_unauth, _clsk, _fbp , _uetsid, _uetvid, _gcl_au | Primários
google.com | DV, NID, 1P_JAR, __Secure-3PSIDCC, __Secure-1PAPISID, SAPISID , HSID, __Secure-1PSID, SIDCC, SID, APISID, __Secure-, __Secure-3PAPISID, SSID, OTZ | De terceiros
.google.ru | __Secure-3PAPISID, SAPISID, SAPISID, HSID, __Secure-3PSID, __Secure-1PSID, SID, SSID, __Secure-1PAPISID, NID | De terceiros
retagro.com | user_id | De terceiros
citydsp.com | userId | De terceiros
pinterest.com | _pinterest_sess | De terceiros
getblue.io | ckid, shash, cftoken, cfid, JSESSIONID,  | De terceiros
facebook.com | fr | De terceiros
doubleclick.net | DSID, IDE | De terceiros
cnt.my | xcntU, xcntID | De terceiros
clarity.ms | MUID, SM | De terceiros
bing.com | SRM_B, SRM_I, MUID | De terceiros
