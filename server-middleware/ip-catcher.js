export default function (req, res, next) {
  const headers = (req && req.headers) ? Object.assign({}, req.headers) : {}
  const xForwardedFor = headers['x-forwarded-for']
  const xRealIp = headers['x-real-ip']
  const ip = xRealIp || xForwardedFor || req.connection.remoteAddress || req.socket.remoteAddress
  req.userIp = ip;
  next()
}
