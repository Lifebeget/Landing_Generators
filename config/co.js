import message from '../i18n/es-mx';

// Колумбия
export default {
  // Плашка распродажи
  saleBanner: {
    endDate: '',
    cookieName: '',
    class: '',
    eternal: false,
    saleLink: '',
    eternalLink: '',
  },

  // Локализация
  localeName: 'es-mx',
  utcOffset: -3,
  message,
  numberFormat: {
    currency: {
      style: 'currency',
      currency: 'COP',
      currencyDisplay: 'symbol',
    },
    currencyNoCents: {
      style: 'currency',
      currency: 'COP',
      currencyDisplay: 'symbol',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    },
    decimal: {
      style: 'decimal',
    },
  },
  defaultPhoneMask: 'co',

  newDomainName: 'https://ebac.com.co', // домен, который будет заменять домен у некоторых ссылок, приходящих из cms. 1g5jk12

  lmsUrl: 'https://lms.ebaconline.com.br/',
  lmsLoginUrl: 'https://lms.ebaconline.com.br/login/',

  // Webinars
  withWebinars: false,
  webinarsUrl: '',
  // Blog
  withBlog: true,
  tildaBlog: true,
  blogUrl: 'https://ebac.com.co/blog',
  getBlogsUrl: 'https://ebac.com.co/api/tilda-main-blog',
  blogLinks: [
    {
      href: 'https://ebac.com.co/blog#!/tab/332850082-2',
      key: 'template.design',
    },
    {
      href: 'https://ebac.com.co/blog#!/tab/332850082-3',
      key: 'template.gaming',
    },
  ],

  withNewCoursesSection: false,

  feedbackUrl: 'https://8yo6k21hf4.execute-api.sa-east-1.amazonaws.com/Prod/subscribe/register',
  feedbackContentType: 'application/json',
  leadFormUrl: 'https://gdd.ebaconline.com.br/exec',
  webinarsCreateLeadUrl: 'application/json',
  webinarsCreateLeadContentType: 'application/x-www-form-urlencoded',
  getPaymentLink: '',

  // Source id
  sourceId: {
    index: 10003,
    sale: null,
    saleBasket: null,
  },

  // Documents
  documents: {
    privacyNoticeStudents: 'https://ebac.mx/upload/cms/ebac_aviso_de_privacidad_integral_alumnos.pdf',
    privacyNoticeTeachers: 'https://ebac.mx/upload/cms/ebac_aviso_de_privacidad_integral_profesores.pdf',
    termsAndConditions: 'https://ebac.mx/upload/cms/ebac_tyc_alumnos_pag_cursos.pdf',
  },
  footerDocuments: ['privacyNoticeStudents', 'privacyNoticeTeachers', 'termsAndConditions'],
  formDocument: 'privacyNoticeStudents', // documents.privacyNoticeStudents
  formWithCheckbox: false,

  // Social
  phone: '',
  phoneLink: '',
  whatsApp: '',
  whatsAppLink: '',
  instagramLink: 'https://www.instagram.com/ebac_mexico/?hl=es',
  facebookLink: 'https://www.facebook.com/EbacMexico',
  youtubeLink: 'https://www.youtube.com/channel/UCVqYXk6kbrdcDbmPtOw9v4g',
  linkedInLink: '',

  // Vacancies
  vacancies: {
    locations: [
      {
        slug: 'sao-paulo',
        value: 'São Paulo',
        label: 'São Paulo',
      },
      {
        slug: 'remote',
        value: 'Remote',
        label: 'Remote',
      },
    ],
  },
  externalVacanciesLink: '',

  // Global head
  head: {
    lang: 'es',
    meta: [
      { name: 'facebook-domain-verification', content: 'jsfm2c5v81sggy4zxw03r5y0dobbqj' },
    ],
    link: [],
    script: [
      // {
      //   innerHTML: `(function(a,m,o,c,r,m){a[m]={id:"55739",hash:"1ff7300d2677990955830635bd87352e4d8646284065bebd0d2b4738baf4a888",locale:"en",inline:true,setMeta:function(p){this.params=(this.params||[]).concat([p])}};a[o]=a[o]||function(){(a[o].q=a[o].q||[]).push(arguments)};var d=a.document,s=d.createElement('script');s.async=true;s.id=m+'_script';s.src='https://gso.amocrm.com/js/button.js?1630666958';d.head&&d.head.appendChild(s)}(window,0,'amoSocialButton',0,0,'amo_social_button'));`,
      //   type: 'text/javascript',
      //   async: true,
      // },
    ],
    __dangerouslyDisableSanitizers: ['script'],
  },

  bodyScripts: [],

  // career-center-students script
  careerCenterStudentsScripts: [],

  gtm: {
    debug: false,
    id: 'GTM-T5D6TQQ',
    autoInit: true,
    enabled: true,
    pageTracking: true,
    scriptDefer: true,
  },
}
