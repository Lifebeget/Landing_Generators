import message from '../i18n/es-mx';

// Мексика
export default {
  // Плашка распродажи
  saleBanner: {
    endDate: '2022-06-01T04:59:00Z',
    cookieName: 'sale-banner-230522',
    class: 'sale-banner_230522',
    eternal: false,
    saleLink: 'https://ebac.mx/cursos',
    eternalLink: 'https://ebac.mx/cursos',
  },

  // Локализация
  localeName: 'es-mx',
  utcOffset: -5,
  message,
  numberFormat: {
    currency: {
      style: 'currency',
      currency: 'MXN',
      currencyDisplay: 'symbol',
    },
    currencyNoCents: {
      style: 'currency',
      currency: 'MXN',
      currencyDisplay: 'symbol',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    },
    decimal: {
      style: 'decimal',
    },
  },
  defaultPhoneMask: 'mx',

  newDomainName: '', // домен, который будет заменять домен у некоторых ссылок, приходящих из cms. 1g5jk12

  lmsUrl: 'https://lms.ebac.mx/',
  lmsLoginUrl: 'https://lms.ebac.mx/login',

  // Webinars
  withWebinars: true,
  webinarsUrl: 'webinars/',
  // Blog
  withBlog: true,
  tildaBlog: true,
  blogUrl: 'https://ebac.mx/blog',
  getBlogsUrl: 'https://ebac.mx/api/tilda-main-blog',
  blogLinks: [
    {
      href: 'https://ebac.mx/blog#!/tab/332850082-2',
      key: 'template.design',
    },
    {
      href: 'https://ebac.mx/blog#!/tab/332850082-3',
      key: 'template.gaming',
    },
  ],

  withNewCoursesSection: false,

  feedbackUrl: 'https://ebac.mx/api/subscriptions/create',
  feedbackContentType: 'application/x-www-form-urlencoded',
  leadFormUrl: 'https://ebac.mx/api/subscriptions/create',
  webinarsCreateLeadUrl: 'https://kzwmxrqypl.execute-api.sa-east-1.amazonaws.com/Prod/mx/subscribe/webinar',
  webinarsCreateLeadContentType: 'application/json',
  getPaymentLink: 'https://ebac.mx/api/subscriptions/getPaymentLink',
  checkSubscriptionsLink: 'https://api.lms.ebac.mx/subscriptions/is-mine',

  // Source id
  sourceId: {
    index: 10003,
    sale: 11000,
    saleBasket: 11000,
  },

  // Documents
  documents: {
    privacyNoticeStudents: 'https://ebac.mx/upload/cms/ebac_aviso_de_privacidad_integral_alumnos.pdf',
    privacyNoticeTeachers: 'https://ebac.mx/upload/cms/ebac_aviso_de_privacidad_integral_profesores.pdf',
    termsAndConditions: 'https://ebac.mx/upload/cms/ebac_tyc_alumnos_pag_cursos.pdf',
  },
  footerDocuments: ['privacyNoticeStudents', 'privacyNoticeTeachers', 'termsAndConditions'],
  formDocument: 'privacyNoticeStudents', // documents.privacyNoticeStudents
  formWithCheckbox: true,

  // Social
  phone: '+52 55 9225-2629',
  phoneLink: 'tel:+525592252629',
  whatsApp: '+52 55 9225-2629',
  whatsAppLink: 'https://wa.me/525592252629',
  instagramLink: 'https://www.instagram.com/ebac_latam',
  facebookLink: 'https://www.facebook.com/EbacMexico',
  youtubeLink: 'https://www.youtube.com/channel/UCVqYXk6kbrdcDbmPtOw9v4g',
  linkedInLink: '',

  // Vacancies
  vacancies: {
    locations: [
      {
        slug: 'mexico-city',
        value: 'São Paulo',
        label: 'Ciudad de México',
      },
      {
        slug: 'remote',
        value: 'Remote',
        label: 'Remote',
      },
    ],
  },
  externalVacanciesLink: 'https://jobs.kenoby.com/ebaconline-mx',

  // Global head
  head: {
    lang: 'es',
    meta: [
      { name: 'facebook-domain-verification', content: 'xxovr0wpum73urzqw7s1xn4rzw3gi2' },
      { name: 'google-site-verification', content: 'yzzTXNX8OXD7QrGXUwIRewJQdvFo53rDEAiKmqGOa_4' },
    ],
    link: [],
    script: [
      // {
      //   innerHTML: `(function(a,m,o,c,r,m){a[m]={id:"55739",hash:"1ff7300d2677990955830635bd87352e4d8646284065bebd0d2b4738baf4a888",locale:"en",inline:true,setMeta:function(p){this.params=(this.params||[]).concat([p])}};a[o]=a[o]||function(){(a[o].q=a[o].q||[]).push(arguments)};var d=a.document,s=d.createElement('script');s.async=true;s.id=m+'_script';s.src='https://gso.amocrm.com/js/button.js?1630666958';d.head&&d.head.appendChild(s)}(window,0,'amoSocialButton',0,0,'amo_social_button'));`,
      //   type: 'text/javascript',
      //   async: true,
      // },
    ],
    __dangerouslyDisableSanitizers: ['script'],
  },

  bodyScripts: [
    {
      src: 'https://cdn.jsdelivr.net/npm/vue@2.6.14',
      async: false,
    },
    {
      src: 'https://ebac.mx/custom/cookie-consent/cookie-consent-init.js',
      async: false,
    },
    {
      src: 'https://ebac.mx/custom/cookie-consent/cookie-consent.min.js',
      async: false,
    },
  ],

  // career-center-students script
  careerCenterStudentsScripts: [],

  gtm: {
    debug: false,
    id: 'GTM-TQR2Q6B',
    autoInit: true,
    enabled: true,
    pageTracking: true,
    scriptDefer: true,
  },
}
