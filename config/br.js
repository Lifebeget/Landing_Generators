import message from '../i18n/pt-br';

// Бразилия
export default {
  // Плашка распродажи
  saleBanner: {
    endDate: '2022-03-26T02:59:00Z',
    cookieName: 'sale-banner-090322',
    class: 'sale-banner_310322',
    eternal: true,
    saleLink: 'https://ebaconline.com.br/sale/dia-do-consumidor',
    eternalLink: 'https://ebaconline.com.br/sale/sale-de-outono',
  },

  // Локализация
  localeName: 'pt-br',
  utcOffset: -3,
  message,
  numberFormat: {
    currency: {
      style: 'currency',
      currency: 'BRL',
      currencyDisplay: 'symbol',
    },
    currencyNoCents: {
      style: 'currency',
      currency: 'BRL',
      currencyDisplay: 'symbol',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    },
    decimal: {
      style: 'decimal',
    },
  },
  defaultPhoneMask: 'bm',

  newDomainName: '', // домен, который будет заменять домен у некоторых ссылок, приходящих из cms. 1g5jk12

  // LMS
  lmsUrl: 'https://lms.ebaconline.com.br/',
  lmsLoginUrl: 'https://lms.ebaconline.com.br/login/',

  // Webinars
  withWebinars: true,
  webinarsUrl: '/webinars',
  // Blog
  withBlog: true,
  tildaBlog: false,
  blogUrl: '/blog',
  getBlogsUrl: '/public/getBlogPosts',
  blogLinks: [
    {
      href: '/blog#!/tab/238198275-2',
      key: 'template.design',
    },
    {
      href: '/blog#!/tab/238198275-3',
      key: 'template.audioVisual',
    },
    {
      href: '/blog#!/tab/238198275-4',
      key: 'template.marketing',
    },
    {
      href: '/blog#!/tab/238198275-5',
      key: 'template.scheduleAndDate',
    },
    {
      href: '/blog#!/tab/238198275-6',
      key: 'template.games',
    },
  ],

  withNewCoursesSection: true,

  feedbackUrl: 'https://gdd.ebaconline.com.br/exec',
  feedbackContentType: 'application/x-www-form-urlencoded',
  leadFormUrl: 'https://gdd.ebaconline.com.br/exec',
  webinarsCreateLeadUrl: 'https://gdd.ebaconline.com.br/exec',
  webinarsCreateLeadContentType: 'application/x-www-form-urlencoded',
  getPaymentLink: 'https://gdd.ebaconline.com.br/api/get_payment_link',

  // Source id
  sourceId: {
    index: 8,
    sale: 132,
    saleBasket: 131,
  },

  // Documents
  documents: {
    serviceAgreementAndPrivacyPolicy: 'https://ebaconline.com.br/upload/cms/service_agreement_and_privacy_policy.pdf',
  },
  footerDocuments: ['serviceAgreementAndPrivacyPolicy'],
  formDocument: 'serviceAgreementAndPrivacyPolicy', // documents.serviceAgreementAndPrivacyPolicy
  formWithCheckbox: true,

  // Social
  phone: '+55 (11) 3030-3200',
  phoneLink: 'tel:+551130303200',
  whatsApp: '+55 (11) 4200-2991',
  whatsAppLink: 'https://wa.me/551142002991',
  instagramLink: 'https://www.instagram.com/e.b.a.c/',
  facebookLink: 'https://www.facebook.com/escolabritanicadeartescriativas/',
  youtubeLink: 'https://www.youtube.com/EBACoficial',
  linkedInLink: 'https://linkedin.com/school/ebac-escola-britanica-artes-criativas',

  // Vacancies
  vacancies: {
    locations: [
      {
        slug: 'sao-paulo',
        value: 'São Paulo',
        label: 'São Paulo',
      },
      {
        slug: 'remote',
        value: 'Remote',
        label: 'Remote',
      },
    ],
  },
  externalVacanciesLink: 'https://jobs.kenoby.com/ebaconline',

  //b2b landing link
  b2bLink: 'https://ebaconline.com.br/sobre/para-empresas',

  // Global head
  head: {
    lang: 'pt',
    meta: [],
    link: [],
    script: [],
  },

  bodyScripts: [
    {
      src: 'https://cdn.jsdelivr.net/npm/vue@2.6.14',
      async: false,
    },
    {
      src: 'https://ebaconline.com.br/custom/cookie-consent/cookie-consent-init.js',
      async: false,
    },
    {
      src: 'https://ebaconline.com.br/custom/cookie-consent/cookie-consent.min.js',
      async: false,
    },
  ],

  // career-center-students script
  careerCenterStudentsScripts: [
    // {
    //   innerHTML: `
    //     var yWidgetSettings = {
    //       buttonColor : '#1C84C6',
    //       buttonPosition : 'bottom right',
    //       buttonAutoShow : true,
    //       buttonText : 'Online booking',
    //       formPosition : 'right'
    //     };
    //   `,
    //   type: 'text/javascript',
    // },
  ],

  gtm: {
    debug: false,
    id: 'GTM-MD8LLQM',
    autoInit: true,
    enabled: true,
    pageTracking: true,
    scriptDefer: true,
  }
}
