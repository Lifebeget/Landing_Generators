import br from './br';
import mx from './mx';
import ar from './ar';
import cl from './cl';
import pe from './pe';
import co from './co';

const CONFIGS = {
  br,
  mx,
  ar,
  cl,
  pe,
  co,
};

export default (CURRENT_COUNTRY) => ({
  common: {
    currentCountry: CURRENT_COUNTRY,
    ...Object.values(CONFIGS).reduce((acc, value) => {
      if (!acc.locales.includes(value.localeName)) acc.locales.push(value.localeName);
      if (!acc.numberFormat.hasOwnProperty(value.localeName)) acc.numberFormat[value.localeName] = value.numberFormat;
      if (!acc.messages.hasOwnProperty(value.localeName)) acc.messages[value.localeName] = value.message;
      return acc;
    }, {
      locales: [],
      numberFormat: {},
      messages: {}
    }),
  },
  current: CONFIGS[CURRENT_COUNTRY],
  ...CONFIGS,
})

