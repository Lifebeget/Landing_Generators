export const state = () => ({
  sale: null,
});

export const mutations = {
  setSale(state, sale) {
    state.sale = sale;
  },
}
