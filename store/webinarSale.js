export const state = () => ({
  dialog: false,
});

export const mutations = {
  setDialog(state, status) {
    state.dialog = status
  }
}