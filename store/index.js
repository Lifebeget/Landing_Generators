export const state = () => ({
  host: '',
  isLandingPage: false,
})

export const mutations = {
  SET_HOST (state, payload) {
    state.host = payload
  },
  setIsLandingPage(state, isLandingPage) {
    state.isLandingPage = isLandingPage;
  },
}

export const actions = {
  async nuxtServerInit ({ commit }, { req }) {
    commit('SET_HOST', req.headers.host)
  }
}
