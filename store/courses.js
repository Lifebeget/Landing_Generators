export const state = () => ({
  courses: [],
  professions: [],
  coursesAll: [],
  professionsAll: [],
  coursesTotal: 0,
  professionsTotal: 0,
  coursesCanLoadMore: false,
  professionsCanLoadMore: false,
  directions: [],
  comingSoonDirections: [],
  activeDirections: [],
  coursesAndProfessions: [],

  isShowedMobileFilter: false,
});

export const mutations = {
  setCourses(state, courses) {
    state.courses = courses;
  },
  setProfessions(state, professions) {
    state.professions = professions;
  },
  setCoursesAll(state, coursesAll) {
    state.coursesAll = coursesAll;
  },
  setProfessionsAll(state, professionsAll) {
    state.professionsAll = professionsAll;
  },
  setCoursesTotal(state, total) {
    state.coursesTotal = total;
  },
  setProfessionsTotal(state, total) {
    state.professionsTotal = total;
  },
  setProfessionsCanLoadMore(state, professionsCanLoadMore) {
    state.professionsCanLoadMore = professionsCanLoadMore;
  },
  setCoursesCanLoadMore(state, coursesCanLoadMore) {
    state.coursesCanLoadMore = coursesCanLoadMore;
  },
  setDirections(state, directions) {
    state.directions = directions;

    state.comingSoonDirections = directions.filter(direction => direction.sort === 777);
    state.activeDirections = directions.filter(direction => direction.sort !== 777);
  },
  setIsShowedMobileFilter(state, isShowedMobileFilter) {
    state.isShowedMobileFilter = isShowedMobileFilter;
  },
  setCoursesAndProfessions(state, payload) {
    state.coursesAndProfessions = payload.AllData.filter(itemFirst => !payload.filterData.some(itemSecond => itemFirst.id === itemSecond.id));
  },
};
