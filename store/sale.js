export const state = () => {
  const detailProductDialogDefault = {
    loading: true,
    product: null,
    productType: '',
    content: {
      months: 0,
      title: '',
      description: '',
      program: [],
      skills: [],
      img: '',
      trailerUrl: '',
      courseProgram: null,
      teachers: [],
    },
  };

  return {
    sale: null,

    courses: [],
    professions: [],
    coursesTotal: 0,
    professionsTotal: 0,
    coursesCanLoadMore: false,
    professionsCanLoadMore: false,
    directions: [],

    isShowedMobileFilter: false,
    isDisplayDetailProductDialog: false,
    detailProductDialogDefault,
    detailProductDialog: detailProductDialogDefault,
    basketDialog: false,
  }
};

export const mutations = {
  setSale(state, sale) {
    state.sale = sale;
  },
  setCourses(state, courses) {
    state.courses = courses;
  },
  setProfessions(state, professions) {
    state.professions = professions;
  },
  setCoursesTotal(state, total) {
    state.coursesTotal = total;
  },
  setProfessionsTotal(state, total) {
    state.professionsTotal = total;
  },
  setProfessionsCanLoadMore(state, professionsCanLoadMore) {
    state.professionsCanLoadMore = professionsCanLoadMore;
  },
  setCoursesCanLoadMore(state, coursesCanLoadMore) {
    state.coursesCanLoadMore = coursesCanLoadMore;
  },
  setDirections(state, directions) {
    state.directions = directions;
  },
  setIsShowedMobileFilter(state, isShowedMobileFilter) {
    state.isShowedMobileFilter = isShowedMobileFilter;
  },
  setIsDisplayDetailProductDialog(state, isDisplayDetailProductDialog) {
    state.isDisplayDetailProductDialog = isDisplayDetailProductDialog;
  },
  setDetailProductDialog(state, detailProductDialog) {
    state.detailProductDialog = detailProductDialog;
  },
  setBasketDialog(state, basketDialog) {
    state.basketDialog = basketDialog;
  }
}
